package src.utils
{
	import flash.geom.Point;
	import nape.geom.Vec2;
	import src.game.core.Universe;
	
	public class AntMath extends Object
	{
		public static const TO_RADIANS:Number = 0.01745329251994329576923690768489;
		public static const TO_DEGREES:Number = 57.295779513082320876798154814105;
		public static const PI_2:Number = 6.283185307179586476925286766559;
		
		/**
		 * Возвращает случайное число из заданного диапазона.
		 * 
		 * @param	lower	 Наименьшее значение диапазона.
		 * @param	upper	 Наибольшее значение диапазона.
		 */
		public static function randomRangeInt(lower:int, upper:int):int
		{
			return int(Math.random() * (upper - lower + 1)) + lower;
		}
		
		public static function randomRangeNumber(lower:Number, upper:Number):Number
		{
			return Math.random() * (upper - lower) + lower;
		}
		
		public static function getSign(value:Number):int
		{
			if (value > 0) return 1;
			if (value == 0) return 0;
			if (value < 0) return -1;
			return 0;
		}
		
		/**
		 * Вращает точку вокруг заданной оси.
		 * 
		 * @param	x	 Значение X точки которую необходимо повернуть.
		 * @param	y	 Значение Y точки которую необходимо повернуть.
		 * @param	pivotX	 Значение X точки оси (вокруг которой необходимо повернуть).
		 * @param	pivotY	 Значение Y точки оси (вокруг которой необходимо повернуть).
		 * @param	angle	 Угол на который необходимо повернуть (в градусах).
		 * 
		 * @return		Возвращает новые координаты точки с учетом заданного угла.
		 */
		static public function rotatePointDeg(x:Number, y:Number, pivotX:Number, pivotY:Number, angle:Number):Point
		{
			var p:Point = new Point();
			var radians:Number = angle * TO_RADIANS;
			var dx:Number = x - pivotX;
			var dy:Number = pivotY - y;
			p.x = pivotX + Math.cos(radians) * dx - Math.sin(radians) * dy;
			p.y = pivotY - (Math.sin(radians) * dx + Math.cos(radians) * dy);
			return p;
		}
		
		/**
		 * Возвращает дистанцию между двумя точками.
		 * 
		 * @param	x1	 Положение первой точки по x.
		 * @param	y1	 Положение первой точки по y.
		 * @param	x2	 Положение второй точки по x.
		 * @param	y2	 Положение второй точки по y.
		 * 
		 * @return		Возвращает дистанцию между двумя точками.
		 */
		public static function distance(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			var dx:Number = x2 - x1;
			var dy:Number = y2 - y1;
			
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		/**
		 * Возвращает угол между двумя точками.
		 * 
		 * @param	x1	 Положение первой точки по x.
		 * @param	y1	 Положение первой точки по y.
		 * @param	x2	 Положение второй точки по x.
		 * @param	y2	 Положение второй точки по y.
		 * @param	norm	 Нормализация угла.
		 * 
		 * @return		Возвращает угол между двумя точками (в радианах).
		 */
		public static function angle(x1:Number, y1:Number, x2:Number, y2:Number, norm:Boolean = true):Number
		{
			var angle:Number = Math.atan2(y2 - y1, x2 - x1);
			
			if (norm)
			{
				if (angle < 0) angle = PI_2 + angle;	
				else if (angle >= PI_2) angle = angle - PI_2;
			}
			
			return angle;
		}
		
		public static function normalizeAngle(angle:Number):Number
		{
			if (angle < 0)
			{
				angle = Math.PI * 2 + angle;	
			}
			else if (angle >= Math.PI * 2)
			{
				angle = angle - Math.PI * 2;
			}
			
			return angle;
		}
		
		public static function getRotAngle(aX, aY, bX, bY, aR, rS):Number
		{
			var mAngle:Number = Math.atan2(bY - aY, bX - aX) * TO_DEGREES;
			var dAngle:Number = aR - mAngle;			
			
			if (dAngle > 180) dAngle = -360 + dAngle;
			else if (dAngle < -180) dAngle = 360 + dAngle;
			
			if (Math.abs(dAngle) < rS) 
			{
				return -dAngle;
			}
			else if (dAngle > 0)
			{
				return -rS;
			}
			else 
			{
				return rS;	
			}		
		}
		
		/**
		 * Возвращает процент значения current от общего значения total.
		 * 
		 * @param current - текущее значение.
		 * @param total - общее значение.
		 * 
		 * @return percent.
		 */
		public static function toPercent(current:Number, total:Number):Number
		{
			return (current / total) * 100;
		}
		
		/**
		 * Возвращает текущее значене исходя из процентного соотношения к общему числу.
		 * 
		 * @param percent - текущий процент.
		 * @param total - общее значение.
		 * 
		 * @return возвращает текущее значение.
		 */
		public static function fromPercent(percent:Number, total:Number):Number
		{
			return (percent * total) / 100;
		}
		
		public static function getMoveVector(speed:Number, angle:Number):Vec2
		{
			angle = angle * TO_RADIANS;
			
			return new Vec2(speed * Math.cos(angle), speed * Math.sin(angle));			
		}
		
		public static function getRotVector(vec:Point, angle:Number):Vec2
		{
			angle = angle * TO_RADIANS;
			
			return new Vec2(vec.x * Math.cos(angle) - vec.y * Math.sin(angle), vec.x * Math.sin(angle) + vec.y * Math.cos(angle));
		}
		
		public static function getRandomPointInRadius(radius:Number):Vec2
		{
			return getMoveVector(randomRangeInt(0, radius), randomRangeInt(0, 359));
		}
	}

}