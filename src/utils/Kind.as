package src.utils 
{
	import flash.display.MovieClip;
	import src.game.core.Universe;
	import flash.utils.ByteArray; 
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Kind 
	{
		private static var _nextIdentifier:int = 0;
		
		public static const OR:int = 1;
		public static const AND:int = 2;
		
		public static const GROUP_NONE:int = 0;
		public static const GROUP_A:int = 100;
		public static const GROUP_B:int = 200;
		public static const GROUP_N:int = 300;
		
		
		public static const T_BG_TILE:int = 1;
		public static const T_HEINKEL_1:int = 2;
		public static const T_HEINKEL_2:int = 3;
		public static const T_HEINKEL_3:int = 4;
		public static const T_HEINKEL_4:int = 5;
		public static const T_HEINKEL_5:int = 6;
		public static const T_HORTEN:int = 13;
		public static const T_DORNIER:int = 14;
		public static const T_COIN:int = 7;
		public static const T_MEDAL:int = 8;
		public static const T_LIVE:int = 9;
		public static const T_SHIELD:int = 10;
		public static const T_BONUS:int = 11;
		public static const T_HERO:int = 12;
		public static const T_BOSS_1:int = 15;
		public static const T_COLIBRI:int = 16;
		
		public static const SCREEN_MAIN_MENU:int = 1;
		public static const SCREEN_GAME:int = 2;
		public static const SCREEN_MARKET:int = 3;
		public static const SCREEN_LEVEL_SELECT:int = 4;
		public static const PANEL_GAME_INFO:int = 5;
		public static const PANEL_LEVEL_COMPLETE:int = 6;
		public static const PANEL_LEVEL_FAILED:int = 7;
		public static const PANEL_PAUSE:int = 8;
		
		
		public static const S_BUTTON_CLICK:int = 1;
		public static const S_COIN_PICKUP:int = 2;
		public static const S_EXPLOSION_E100:int = 3;
		public static const S_EXPLOSION_E200:int = 4;
		public static const S_BONUS_LIVE:int = 5;
		public static const S_BONUS_SHIELD:int = 10;
		public static const S_BUY:int = 6;
		public static const S_GAME:int = 7;
		public static const S_MENU:int = 8;
		public static const S_MEDAL:int = 9;
		
		
		public static function dot(x:int, y:int):void
		{
			var d:MovieClip = new Dot_mc();
			Universe.getInstance().overgroundContainer.addChild(d);
			d.x = x;
			d.y = y;
		}
		
		public static function clone(source:Object):* 
		{ 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject(source); 
			myBA.position = 0; 
			return(myBA.readObject()); 
		}
	}

}