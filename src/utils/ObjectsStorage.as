package src.utils 
{
	import src.framework.id.IdFilter;
	import src.game.objects.BasicObject;
	/**
	 * ...
	 * @author ...
	 */
	public class ObjectsStorage extends Object
	{
		public var objects:Array;
		public var onRemove:Array;
		
		public function ObjectsStorage() 
		{
			objects = [];
			onRemove = [];
		}
		
		public function add(object:BasicObject):void
		{
			var k:int = objects.indexOf(object);
			if (k == -1) objects.push(object);
		}
		
		public function remove(object:BasicObject):void
		{
			onRemove.push(object);
		}
		
		public function update():void
		{
			for (var i:int = 0; i < onRemove.length; i++)
			{
				var k:int = objects.indexOf(onRemove[i]);
				if ( k !== -1)
				{
					objects.splice(k, 1);
				}
			}
			
			onRemove = [];
			
			var l:int = objects.length;
			for (var i:int = 0; i < l; i++)
			{				
				objects[i].update();
				objects[i].afterUpdate();
			}
			
			//trace(objects.length);
		}
		
		public function getObjectsByTagOR(tags:Array):Array
		{
			var objs:Array = [];
			for (var i:int = 0; i < objects.length; i++)
			{
				var o:BasicObject = objects[i] as BasicObject;
				if (o !== null)
				{
					if (o.id.checkOR(tags)) objs.push(o);
				}
			}
			return objs;
		}
		
		public function destroyAll():void
		{
			var l:int = objects.length;
			for (var i:int = 0; i < l; i++)
			{				
				if(objects[i].isExist) objects[i].destroy();
			}
			
			for (var i:int = 0; i < onRemove.length; i++)
			{
				var k:int = objects.indexOf(onRemove[i]);
				if ( k !== -1)
				{
					objects.splice(k, 1);
				}
			}
		}
	}
}