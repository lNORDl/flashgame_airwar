package src.utils 
{
	import src.game.objects.BasicBehavior;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BehaviorsStorage 
	{
		private var behaviors:Array;
		private var onRemove:Array;
		
		public function BehaviorsStorage() 
		{
			behaviors = [];
			onRemove = [];
		}
		
		public function add(behavior:BasicBehavior):void
		{
			var k:int = behaviors.indexOf(behavior);
			if (k == -1) behaviors.push(behavior);
		}
		
		public function remove(behavior:BasicBehavior):void
		{
			var k:int = behaviors.indexOf(behavior);
			if (k !== -1) onRemove.push(behavior);
		}
		
		public function update():void
		{
			var l:int = behaviors.length;
			for (var i:int = 0; i < l; i++)
			{				
				behaviors[i].update();
			}
			
			for (var i:int = 0; i < onRemove.length; i++)
			{
				var k:int = behaviors.indexOf(onRemove[i]);
				if ( k !== -1)
				{
					behaviors.splice(k, 1);
				}
			}
			
			onRemove = [];
		}
		
		public function destroy():void
		{
			
		}
	}
}