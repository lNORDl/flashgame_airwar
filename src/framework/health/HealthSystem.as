package src.framework.health 
{
	import flash.geom.Point;
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.objects.BasicObject;
	import src.game.objects.enemies.BasicEnemy;
	import src.game.objects.enemies.Boss_1;
	import src.game.objects.hero.Hero;
	/**
	 * ...
	 * @author NORD
	 */
	public class HealthSystem extends Object
	{
		public var maxHealth:int;
		public var curHealth:int;
		
		private var object:IDestoyable;	
		
		public var isImmortal:Boolean;
		
		public function HealthSystem(o:IDestoyable, mH:int) 
		{
			object = o;
			
			maxHealth = mH;
			curHealth = maxHealth;
			
			isImmortal = false;
		}
		
		public function addDamage(damage:int):void
		{
			if (!isImmortal)
			{
				curHealth -= damage;
				if (object is Hero) (object as Hero).onDamage();
				else if (object is BasicEnemy) (object as BasicEnemy).onDamage();
				else if (object is Boss_1) (object as Boss_1).onDamage();
			}
			
			if (curHealth <= 0) 
			{
				object.liquidate();
				object = null;
			}
		}		
		
		public function addHealth(health:int):void
		{
			curHealth += health;
			
			if (curHealth > maxHealth) curHealth = maxHealth;
		}
		
		public function destroy():void
		{
			object = null;
		}
	}
}