package src.framework.health 
{
	
	/**
	 * ...
	 * @author NORD
	 */
	public interface IDestoyable
	{
		function getHealthSystem():HealthSystem;
		function liquidate():void;
	}
	
}