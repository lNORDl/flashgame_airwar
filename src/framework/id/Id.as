package src.framework.id 
{
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Id extends Object
	{
		private var tags:Array;
		
		public function Id(t:Array)
		{
			tags = t;
		}
		
		public function addTag(t:int):void
		{
			tags.push(t);
		}
		
		public function addTags(t:Array):void
		{
			for (var i:int = 0; i < t.length; i++) 
			{
				tags.push(t[i]);
			}
		}
		
		public function checkOR(t:Array):Boolean
		{
			for (var i:int = 0; i < t.length; i++)
			{
				for (var j:int = 0; j < tags.length; j++)
				{
					if (t[i] == tags[j]) return true;
				}
			}
			
			return false;
		}
	}
}