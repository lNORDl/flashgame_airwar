package src.framework.indecator 
{
	import flash.display.Sprite;
	import src.framework.animation.AntActor;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Indecator extends Sprite
	{		
		private var icons:Array;
		private var count:int;
		
		public function Indecator(name:String, c:int, margin:int) 
		{
			icons = [];
			
			count = c;
			
			var p:int = 0;
			var s:int = -1;
			
			
			for (var i:int = 0; i < count; i++)
			{
				var icon:AntActor = new AntActor(name);
				this.addChild(icon);
				
				if (s == -1) s = icon.width + margin;
				icon.x = p;
				
				p += s;
				
				icons.push(icon);
			}
		}
		
		public function update(c:int):void
		{
			for (var i:int = 0; i < icons.length; i++)
			{
				if (c >= i + 1) icons[i].gotoAndStop(1);
				else icons[i].gotoAndStop(2);
			}
		}		
	}
}