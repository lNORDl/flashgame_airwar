package src.framework.frameTimer 
{
	import src.game.core.Universe;
	/**
	 * ...
	 * @author NORD
	 */
	public class FrameTimer extends Object
	{
		public var isTick:Boolean = false;
		
		private var _defDelay:int = 0;
		private var _curDelay:int = 0;
		private var _isStart:Boolean = true;
		
		public function FrameTimer(dD:int, cD:int = -1) 
		{
			_defDelay = dD;
			_curDelay = (cD == -1)?_defDelay:cD;
			if (isTick) _curDelay = _defDelay;
			
			Universe.getInstance().addTimer(this);
		}
		
		public function update():void
		{
			if (_curDelay !== _defDelay) isTick = false;
			
			if (_isStart)
			{
				if (isTick) _curDelay = 0;
				
				if (_curDelay >= _defDelay)
				{
					isTick = true;
				}
				else
				{
					isTick = false;
					_curDelay ++;
				}	
			}
		}
		
		public function destroy():void
		{
			Universe.getInstance().removeTimer(this);
		}
		
		public function stop():void
		{
			_isStart = false;
		}
		
		public function start():void
		{
			_isStart = true;
		}
		
		public function reset(t:int = 0):void
		{
			isTick = false;
			_curDelay = t;
		}
	}
}