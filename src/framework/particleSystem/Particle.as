package src.framework.particleSystem 
{
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.getDefinitionByName;
	import src.game.core.Universe;
	import src.utils.animation.AntActor;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author NORD
	 */
	public class Particle 
	{		
		public static var cacheSize:int = 800;
		
		private static var _particles:Array = [];
		private static var _cache:Array = [];
		
		private static var PARAMETR_NAME:int = 0;
		private static var PARAMETR_SETTINGS:int = 1;
		private static var PARAMETR_PARTICLES:int = 2;
		
		private static const TO_RADIAN:Number = 0.01745329251994329576923690768489;
		private static const TO_DEGREES:Number = 57.295779513082320876798154814105;
		
		//===============================================
		private var _universe:Universe;
		
		private var _actor:AntActor;
		private var _speed:Number;
		private var _gravity:Number;
		private var _direction:Number;
		private var _live:Number;
		private var _name:String;
		
		private var _speedX:Number;
		private var _speedY:Number;
		private var _rad:Number;
		private var _widthStep:Number;
		private var _heightStep:Number;
		private var _alphaStep:Number;
		
		public var isExist:Boolean = false;
		
		public function Particle() 
		{
			
		}
		
		public function init(name:String, posX:Number, posY:Number, rot:Number):void
		{
			var i:int = findParticle(name);
			if (i !== -1)
			{
				_universe = Universe.getInstance();
				
				var settings:ParticleSettings = _particles[i][PARAMETR_SETTINGS];
				
				_name = name;
				
				_actor = new AntActor();
				_actor.addAnimFromCache(settings.actorName, null, true);
				_actor.play();
				//_actor.smoothing = true;
				
				_universe.overgroundContainer.addChild(_actor);
				_actor.x = posX;
				_actor.y = posY;
				
				_direction = rot + AntMath.randomRangeNumber(-2, 2);
				_rad = _direction * TO_RADIAN;
				
				_speed = AntMath.randomRangeNumber(settings.speedMin, settings.speedMax);
				_gravity = AntMath.randomRangeNumber(settings.gravityMin, settings.gravityMax);
				_live = AntMath.randomRangeInt(settings.liveMin, settings.liveMax)
				
				var sV:Number = AntMath.randomRangeNumber(0, settings.sizeVar);
				_actor.width = settings.sizeStart.x + settings.sizeStart.x * sV;
				_actor.height = settings.sizeStart.y + settings.sizeStart.y * sV;
				_widthStep = ((settings.sizeEnd.x + settings.sizeEnd.x * sV) - (settings.sizeStart.x + settings.sizeStart.x * sV)) / _live;
				_heightStep =((settings.sizeEnd.y + settings.sizeEnd.y * sV) - (settings.sizeStart.y + settings.sizeStart.y * sV)) / _live;
				
				_actor.alpha = settings.alphaStart / 100;
				_alphaStep = (settings.alphaEnd - settings.alphaStart) / _live / 100;
				
				_particles[i][PARAMETR_PARTICLES].push(this);
				isExist = true;
			}
		}
		
		public function update():void
		{
			if (isExist)
			{
				_speedX = _speed * Math.cos(_direction * TO_RADIAN);
				_speedY = _speed * Math.sin(_direction * TO_RADIAN);
				
				_actor.x += _speedX;
				_actor.y += _speedY;
				
				_actor.width += _widthStep;
				_actor.height += _heightStep;
				_actor.alpha += _alphaStep;
				
				_speed -= _gravity;
				
				if (_live == 0) destroy();
				_live --;
			}
		}
		
		public function destroy():void
		{
			if (isExist)
			{
				_actor.free();
				_actor = null;
				
				_universe = null;
				var i:int = findParticle(_name);
				
				_particles[i][PARAMETR_PARTICLES].splice(_particles[i][PARAMETR_PARTICLES].indexOf(this), 1);
				
				if(_cache.length < cacheSize) _cache.push(this);
				
				isExist = false;
			}
		}
		
		//============================================================================================================
		
		public static function add(name:String, settings:ParticleSettings):void
		{
			var l:int = _particles.length;
			_particles[l] = [];
			_particles[l][PARAMETR_NAME] = name;
			_particles[l][PARAMETR_SETTINGS] = settings;
			_particles[l][PARAMETR_PARTICLES] = [];
		}
		
		public static function findParticle(name:String):int
		{
			var l:int = _particles.length;
			for (var i:int = 0; i < l; i++) if (_particles[i][PARAMETR_NAME] == name) return i;
			
			return -1;
		}
		
		public static function create(name:String, posX:Number, posY:Number, rot:Number):Particle
		{
			var particle:Particle;
			
			if (_cache.length == 0) particle = new Particle();
			else 
			{
				particle = _cache[_cache.length - 1];
				_cache.length = _cache.length - 1;
			}
			particle.init(name, posX, posY, rot);
			
			return particle;
		}
		
		public static function update():void
		{
			var l:int = _particles.length;
			for (var i:int = 0; i < l; i++)
			{
				for (var j:int = 0; j < _particles[i][PARAMETR_PARTICLES].length; j++)
				{
					_particles[i][PARAMETR_PARTICLES][j].update();
				}
			}
		}		
	}
}