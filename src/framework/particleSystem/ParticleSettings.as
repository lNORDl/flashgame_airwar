package src.framework.particleSystem 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author NORD
	 */
	public class ParticleSettings 
	{
		public var actorName:String;
		public var liveMin:int;
		public var liveMax:int;
		public var speedMin:Number;
		public var speedMax:Number;
		public var gravityMin:Number;
		public var gravityMax:Number;
		public var sizeStart:Point;
		public var sizeEnd:Point;
		public var sizeVar:Number;
		public var alphaStart:Number;
		public var alphaEnd:Number;
		
		public function ParticleSettings() 
		{
			
		}		
	}
}