package src.framework.animation
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * AntActor класс графическая сущность работающая растровыми анимациями.
	 * 
	 * @langversion ActionScript 3
	 * @playerversion Flash 9.0.0
	 * 
	 * @author Anton Karlov (http://www.ant-karlov.ru)
	 * @since  01.04.2012
	 */
	public class AntActor extends Sprite
	{
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * Зациклинность анимаций.
		 * Если true то анимация проигрывается по кругу.
		 */
		public var repeat:Boolean;
		
		/**
		 * Обратное воспроизведение.
		 * Если true то анимация воспроизводится в обратном порядке.
		 */
		public var reverse:Boolean;
		
		/**
		 * Скорость воспроизведения, по умолчанию 1.
		 */
		public var animSpeed:Number;
		
		/**
		 * Указатель на метод который может быть вызыван при завершении проигрывания анимации.
		 */
		public var onCompleteCallback:Function = null;

		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected var _bitmap:Bitmap; // Растровое полотно для отображения текущего кадра.
		protected var _smoothing:Boolean; // Сглаживание
		protected var _isPlaying:Boolean; // Проигрывается ли анимация?
		protected var _animations:Array; // Коллекция анимаций
		protected var _curFrame:Number; // Текущий кадр
		protected var _curAnim:AntAnim; // Текущая анимация
		protected var _curAnimName:String; // Имя текущей анимации
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		
		/**
		 * @constructor
		 */
		public function AntActor(s:String = "")
		{
			super();
			
			repeat = true;
			reverse = false;
			animSpeed = 1;
			
			_bitmap = new Bitmap();
			addChild(_bitmap);
			_smoothing = false;
			_isPlaying = false;
			_animations = [];
			_curAnim = null;
			_curFrame = 1;
			_curAnimName = "undefined";
			
			if (s !== "") addAnimFromCache(s, null, true);
		}
		
		/**
		 * @private
		 */
		public function free():void
		{
			stop();
			
			removeChild(_bitmap);
			_bitmap.bitmapData = null;
			_bitmap = null;
			
			_animations.length = 0;
			_animations = null;
			
			_curAnim = null;
			
			if (parent != null)
			{
				parent.removeChild(this);
			}
			
			onCompleteCallback = null;
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * Добавляет новую анимацию.
		 * 
		 * @param	anim	 Указатель на анимацию.
		 * @param	uniqueName	 Имя анимации внутри актера (если не указанно то используется имя анимации == имя класса клипа).
		 * @param	switchToAnim	 Если true то актер сразу же переключится на новую анимацию.
		 */
		public function addAnim(anim:AntAnim, uniqueName:String = null, switchToAnim:Boolean = false):void
		{
			var animName:String = (uniqueName == null) ? anim.name : uniqueName;
			_animations[animName] = anim;
			
			if (switchToAnim || _curAnim == null)
			{
				switchAnim(animName);
			}
		}
		
		/**
		 * Добавляет новую анимацию из кэша анимаций.
		 * 
		 * @param	key	 Идентификатор анимации в кэше анимаций == имя класса клипа.
		 * @param	uniqueName	 Имя анимации внутри актера (если не указанно то используется имя анимации == имя класса клипа).
		 * @param	switchToAnim	 Если true то актер сразу же переключится на новую анимацию.
		 */
		public function addAnimFromCache(key:String, uniqueName:String = null, switchToAnim:Boolean = false):void
		{
			var animName:String = (uniqueName == null) ? key : uniqueName;
			_animations[animName] = AntAnimCache.getInstance().getAnim(key);
			
			if (switchToAnim || _curAnim == null)
			{
				switchAnim(animName);
			}
		}
		
		/**
		 * Переключается на заданную анимацию.
		 * 
		 * @param	animName	 Имя анимации внутри актера.
		 */
		public function switchAnim(animName:String):void
		{
			if (_curAnimName == animName)
			{
				return;
			}
			
			if (_animations[animName] != null)
			{
				_curAnim = _animations[animName];
				_bitmap.x = _curAnim.offsetX;
				_bitmap.y = _curAnim.offsetY;
				_curFrame = 1;
				goto(_curFrame);
				_curAnimName = animName;
			}
			else
			{
				throw new Error("AntActor::switchAnim() - ERROR: Animation \"" + animName +"\" not existing.");
			}
		}
		
		/**
		 * Запускает проигрывание анимации.
		 */
		public function play():void
		{
			if (!_isPlaying)
			{
				addEventListener(Event.ENTER_FRAME, enterFrameHandler, false, 0, true);
				_isPlaying = true;
			}
		}
		
		/**
		 * Останавливает проигрывание анимации.
		 */
		public function stop():void
		{
			if (_isPlaying)
			{
				removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
				_isPlaying = false;
			}
		}
		
		/**
		 * Переходит на указанный кадр и останавливает проигрывание анимации.
		 * 
		 * @param	frame	 Кадр на который необходимо перейти.
		 */
		public function gotoAndStop(frame:Number):void
		{
			_curFrame = (frame <= 0) ? 1 : (frame > totalFrames) ? totalFrames : frame;
			goto(_curFrame);
			stop();
		}
		
		/**
		 * Переходит на указанный кадр и запускает проигрывание анимации.
		 * 
		 * @param	frame	 Кадр на который необходимо перейти.
		 */
		public function gotoAndPlay(frame:Number):void
		{
			_curFrame = (frame <= 0) ? 1 : (frame > totalFrames) ? totalFrames : frame;
			goto(_curFrame);
			play();
		}
		
		/**
		 * Запускает проигрывание анимации со случайного кадра.
		 */
		public function playRandomFrame():void
		{
			gotoAndPlay(Math.ceil(Math.random() * totalFrames));
		}
		
		/**
		 * Переходит к следущему кадру.
		 * 
		 * @param	useSpeed	 Если true то переход к кадру будет выполнен с учетом скорости анимации.
		 */
		public function nextFrame(useSpeed:Boolean = false):void
		{
			useSpeed ? _curFrame += animSpeed : _curFrame++;
			goto(_curFrame);
		}
		
		/**
		 * Переходит к предыдущему кадру.
		 * 
		 * @param	useSpeed	 Если true то переход к кадру будет выполнен с учетом скорости анимации.
		 */
		public function prevFrame(useSpeed:Boolean = false):void
		{
			useSpeed ? _curFrame -= animSpeed : _curFrame--;
			goto(_curFrame);
		}
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * Обработчик анимации.
		 */
		protected function enterFrameHandler(event:Event):void
		{
			if (_curAnim != null)
			{
				if (reverse)
				{
					_curFrame = (_curFrame <= 1) ? totalFrames : _curFrame;
					prevFrame(true);
					if (Math.floor(_curFrame) <= 1)
					{
						_curFrame = 1;
						animComplete();
					}
				}
				else
				{
					_curFrame = (_curFrame >= totalFrames) ? 0 : _curFrame;
					nextFrame(true);
					if (Math.floor(_curFrame) >= totalFrames)
					{
						_curFrame = totalFrames;
						animComplete();
					}
				}
			}
			else
			{
				stop();
			}
		}
		
		/**
		 * Выполняется когда проигрывание анимации завершено.
		 */
		protected function animComplete():void
		{
			if (!repeat)
			{
				stop();
			}
			
			dispatchEvent(new Event(Event.COMPLETE));
			
			if (onCompleteCallback != null)
			{
				(onCompleteCallback as Function).apply(this);
			}
		}
		
		/**
		 * Переход к указанному кадру.
		 * 
		 * @param	frame	 Кадр на который необходимо перейти.
		 */
		protected function goto(frame:Number):void
		{
			var i:int = Math.floor(frame - 1);
			i = (i <= 0) ? 0 : (i >= totalFrames - 1) ? totalFrames - 1 : i;
			_bitmap.bitmapData = _curAnim.frames[i];
			_bitmap.smoothing = _smoothing;
		}
		
		//---------------------------------------
		// GETTER / SETTERS
		//---------------------------------------
		
		/**
		 * Возвращает общее кол-во кадров для текущей анимации.
		 */
		public function get totalFrames():int
		{
			return _curAnim != null ? _curAnim.frames.length : 0;
		}
		
		/**
		 * @private
		 */
		public function get currentFrame():int
		{
			return Math.floor(_curFrame);
		}
		
		/**
		 * Возвращает true если анимация проигрывается.
		 */
		public function get playing():Boolean
		{
			return _isPlaying;
		}
		
		/**
		 * Устанавливает сглаживание для растра.
		 */
		public function set smoothing(value:Boolean):void
		{
			_smoothing = value;
			_bitmap.smoothing = _smoothing;
		}
		
		public function get smoothing():Boolean
		{
			return _smoothing;
		}
		
		/**
		 * Возвращает имя текущей анимации.
		 */
		public function get animName():String
		{
			return _curAnimName;
		}
		
	}

}