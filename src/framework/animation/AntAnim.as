package src.framework.animation
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.utils.getQualifiedClassName;
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	/**
	 * AntAnim класс хранящий в себе только набор кадров и смещения для анимации.
	 * 
	 * @langversion ActionScript 3
	 * @playerversion Flash 9.0.0
	 * 
	 * @author Anton Karlov (http://www.ant-karlov.ru)
	 * @since  01.04.2012
	 */
	public class AntAnim extends Object
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var name:String; // Имя анимации по умолчанию.
		public var frames:Array /* of BitmapData */; // Массив кадров.
		public var offsetX:Number; // Смещение кадров по X относительно оси вращения.
		public var offsetY:Number; // Смещение кадров по Y относительно оси вращения.
		
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
		
		/**
		 * @constructor
		 */
		public function AntAnim()
		{
			super();
			
			name = "undefined";
			frames = [];
			offsetX = 0;
			offsetY = 0;
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		/**
		 * Кэширование клипа из библиотеки.
		 * 
		 * @param	clipName	 Имя класса клипа в библиотеки клипов.
		 */
		public function cacheFromLib(clipClass:Class):void
		{
			name = getQualifiedClassName(clipClass);
			cacheFromClip(new clipClass());
			//cacheFromClip(new (getDefinitionByName(clipName)));
		}
		
		/**
		 * Кэширование из экземпляра клипа.
		 * 
		 * @param	clip	 Клип который необходимо растеризировать.
		 */
		public function cacheFromClip(clip:MovieClip):void
		{
			var r:Rectangle;
			
			// Если размеры кадров заданы вложенным клипом с именем "e_bounds".
			if (clip["e_bounds"])
			{
				var s:Sprite = clip["e_bounds"] as Sprite;
				r = new Rectangle(s.x, s.y, s.width, s.height);
				s.visible = false;
			}
			// Иначе определяем размер кадров самостоятельно.
			else
			{
				//r = clip.getRect(clip); // Старый метод, исходя из размера первого кадра
				r = getFrameSize(clip); // Новый метод, ищет максимальный размер кадра на основе всех кадров
			}
			
			// Если клип пустой
			if (Math.ceil(r.width) == 0 || Math.ceil(r.height) == 0)
			{
				throw new Error("AntAnim::cacheFromClip() - ERROR: Clip \"" + name + "\" is empty.");
				return;
			}
			
			// Кэшируем клип
			var m:Matrix = new Matrix();
			var n:int = clip.totalFrames;
			for (var i:int = 1; i <= n; i++)
			{
				clip.gotoAndStop(i);
				allChildGotoFrame(clip, i);
				var bmpd:BitmapData = new BitmapData(Math.ceil(r.width), Math.ceil(r.height), true, 0x00000000);
				m.identity();
				m.translate(-r.x, -r.y);
				m.scale(clip.scaleX, clip.scaleY);
				bmpd.draw(clip, m);
				frames[frames.length] = bmpd;
			}
			
			offsetX = r.x;
			offsetY = r.y;
		}
		
		/**
		 * Создает новый экземпляр анимации эдентичный текущей.
		 * 
		 * @return		Новый экземпляр анимации.
		 */
		public function clone():AntAnim
		{
			var anim:AntAnim = new AntAnim();
			anim.name = name;
			anim.frames = frames;
			anim.offsetX = offsetX;
			anim.offsetY = offsetY;
			return anim;
		}
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * Рассчитывает максимальный кадра на основе всех кадров клипа.
		 * 
		 * @param	mc	 Клип для которого необходимо получить максимальный размер кадра.
		 * @return		Возвращает максимальный размер кадра для указанного клипа.
		 */
		protected function getFrameSize(mc:MovieClip):Rectangle
		{
			var min:Point = new Point();
			var max:Point = new Point();
			var bounds:Rectangle;
			
			var n:int = mc.totalFrames;
			for (var i:int = 1; i <= n; i++)
			{
				mc.gotoAndStop(i);
				allChildGotoFrame(mc, i);
				bounds = mc.getBounds(mc);
				min.x = Math.min(bounds.topLeft.x, min.x);
				min.y = Math.min(bounds.topLeft.y, min.y);
				max.x = Math.max(bounds.bottomRight.x, max.x);
				max.y = Math.max(bounds.bottomRight.y, max.y);
			}
			
			mc.gotoAndStop(1);
			return new Rectangle(min.x, min.y, max.x - min.x, max.y - min.y);
		}
		
		/**
		 * Переключает кадры указанного клипа и вложенных в него клипов на заданный кадр.
		 * 
		 * @param	mc	 Клип кадры которого необходимо переключить.
		 * @param	frame	 Номер кадра на которых необходимо переключить.
		 */
		protected function allChildGotoFrame(mc:MovieClip, frame:int):void
		{
			var clip:Object;
			var n:int = mc.numChildren;
			for (var i:int = 0; i < n; i++)
			{
				clip = mc.getChildAt(i);
				if (clip is MovieClip)
				{
					allChildGotoFrame(clip as MovieClip, frame);
					clip.gotoAndStop(frame);
				}
			}
		}

	}

}