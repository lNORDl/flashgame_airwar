package src.game.interfaces.buttons 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.objects.hero.HeroParametrs;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonPlayLevel extends BasicButton
	{
		private var n:int;
		
		private var mc:MovieClip;
		private var text:TextField;
		private var lock:MovieClip;
		
		public static function create(w:BasicWindow, posX:int, posY:int, n:int):ButtonPlayLevel
		{
			var button:ButtonPlayLevel = new ButtonPlayLevel();
			button.n = n;
			button.init(w);
			button.x = posX;
			button.y = posY;
			
			return button;
		}
		
		public function ButtonPlayLevel() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			button = new AntActor("ButtonPlayLevel_mc");
			frameNormal = 1;
			frameHover = 2;
			
			
			super.init(w);
			
			
			mc = new ButtonPlayLevelText_mc();
			this.addChild(mc);
			text = mc.getChildByName("Text") as TextField;
			text.text = "";
			
			lock = new ButtonPlayLevelLock_mc();
			this.addChild(lock);
		}
		
		public function updateParametrs():void
		{
			if (n > HeroParametrs.getMaxLevel())
			{
				lock.visible = true;
				text.text = "";
			}
			else
			{
				lock.visible = false;
				text.text = n.toString();
			}
		}
		
		override protected function onClick():void
		{
			if (n <= HeroParametrs.getMaxLevel()) Universe.getInstance().startLevel(n);
		}			
	}
}