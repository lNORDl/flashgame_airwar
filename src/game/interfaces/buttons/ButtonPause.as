package src.game.interfaces.buttons 
{
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.windows.BasicWindow;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonPause extends BasicButton
	{
		
		public function ButtonPause() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			button = new AntActor("ButtonPause_mc");
			frameNormal = 1;
			frameHover = 2;
			
			this.y = 62;
			
			
			super.init(w);
		}
		
		override protected function onClick():void
		{
			Universe.getInstance().switchPause();
		}	
	}
}