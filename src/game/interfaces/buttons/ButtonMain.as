package src.game.interfaces.buttons 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.game.core.Game;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonMain extends BasicButton
	{
		public static var PLAY:int = 1;
		public static var CREDITS:int = 3;
		public static var MORE_GAMES:int = 4;
		
		private var mode:int;
		
		private var mc:MovieClip;
		private var text:TextField;
		
		private var isOrange:Boolean;
		
		public static function create(w:BasicWindow, posX:int, posY:int, mode:int):ButtonMain
		{
			var button:ButtonMain = new ButtonMain();
			button.mode = mode;
			button.init(w);
			button.x = posX;
			button.y = posY;
			
			return button;
		}
		
		public function ButtonMain() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			isOrange = true;
			
			var t:String;
			if (isOrange) t = "ButtonMainOrange_mc";
			else t = "ButtonMainBlue_mc";
			
			button = new AntActor(t);
			frameNormal = 1;
			frameHover = 2;
			
			
			super.init(w);
			
			
			if (isOrange) mc = new ButtonMainOrangeText_mc();
			else mc = new ButtonMainBlueText_mc();
			this.addChild(mc);
			text = mc.getChildByName("Text") as TextField;
			
			if (mode == PLAY) t = "Play";
			if (mode == CREDITS) t = "Credits";
			if (mode == MORE_GAMES) t = "More Games";
			text.text = t;
			
			mc.mouseEnabled = false;
		}
		
		override protected function onClick():void
		{
			if (mode == PLAY) Game.getInstance().windowsManager.switchScreen(Kind.SCREEN_LEVEL_SELECT);
		}			
	}
}