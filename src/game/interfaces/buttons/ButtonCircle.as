package src.game.interfaces.buttons 
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import src.framework.animation.AntActor;
	import src.game.core.Game;
	import src.game.core.Universe;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonCircle extends BasicButton
	{
		public static var GO_MENU:int = 1;
		public static var GO_NEXT:int = 2;
		public static var GO_BACK:int = 3;
		public static var RESTART:int = 4;
		public static var SOUND:int = 5;
		
		private var mode:int;
		private var icon:MovieClip;
		
		public static function create(w:BasicWindow, posX:int, posY:int, mode:int):ButtonCircle
		{
			var button:ButtonCircle = new ButtonCircle();
			button.mode = mode;
			button.init(w);
			button.x = posX;
			button.y = posY;
			
			return button;
		}
		
		public function ButtonCircle() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			button = new AntActor("ButtonCircle_mc");
			frameNormal = 1;
			frameHover = 2;
			
			
			super.init(w);
			
			
			if (mode == 1) icon = new ButtonCircleIconMenu_mc();
			if (mode == 2 || mode == 3) icon = new ButtonCircleIconNext_mc();
			if (mode == 4) icon = new ButtonCircleIconRestart_mc();
			if (mode == 5) icon = new ButtonCircleIconSound_mc();
			if (mode == 3) icon.rotation += 180;
			if (icon !== null) this.addChild(icon);
			icon.stop();
			icon.cacheAsBitmap = true;
		}
		
		override protected function onClick():void
		{
			if (mode == GO_MENU || mode == GO_BACK) Game.getInstance().windowsManager.switchScreen(Kind.SCREEN_MAIN_MENU);
			if (mode == GO_NEXT || mode == RESTART) Universe.getInstance().startLevel(HeroParametrs.getCurrentLevel());
			if (mode == SOUND)
			{
				Game.getInstance().soundManager.paused(Game.getInstance().soundManager.isPlay);
				if (Game.getInstance().soundManager.isPlay) icon.gotoAndStop(1);
				else icon.gotoAndStop(2);
			}
		}	
		
		public function updateParametrs():void
		{
			if (mode == SOUND)
			{
				if (Game.getInstance().soundManager.isPlay) icon.gotoAndStop(1);
				else icon.gotoAndStop(2);
			}
		}
	}
}