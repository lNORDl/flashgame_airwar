package src.game.interfaces.buttons 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import src.framework.animation.AntActor;
	import src.game.core.Game;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicButton extends Sprite
	{
		protected var frameNormal:int;
		protected var frameHover:int;
		
		protected var window:BasicWindow;
		protected var button:AntActor;
		
		protected var clickSound:int = -1;
		
		public function BasicButton() 
		{
			
		}
		
		public function init(w:BasicWindow):void
		{
			window = w;
			window.addButton(this);			
			
			this.addChild(button);
			this.buttonMode = true;
			
			if (clickSound == -1) clickSound = Kind.S_BUTTON_CLICK;
			this.mouseChildren = false;
		}
		
		private function addListeners():void
		{
			this.addEventListener(MouseEvent.CLICK, onMouseClick);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		private function removeListeners():void
		{
			this.removeEventListener(MouseEvent.CLICK, onMouseClick);
			this.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected function onClick():void
		{
			
		}
		
		private function onMouseClick(e:MouseEvent):void 
		{
			if (window.isActive) 
			{
				if ((window.parentWindow == null) || (window.parentWindow !== null && window.parentWindow.isActive))
				{
					//trace("AAAA");
					if(clickSound !== -1) Game.getInstance().soundManager.playSound(clickSound);
					onClick();
				}
			}
		}
		
		private function onMouseOver(e:MouseEvent):void 
		{
			button.gotoAndStop(frameHover);
		}
		
		private function onMouseOut(e:MouseEvent):void 
		{
			button.gotoAndStop(frameNormal);
		}
		
		public function show():void
		{
			addListeners();
		}
		
		public function hide():void
		{
			removeListeners();
		}		
	}
}