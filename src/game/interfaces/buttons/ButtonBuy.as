package src.game.interfaces.buttons 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.interfaces.windows.screens.ScreenMarket;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonBuy extends BasicButton
	{
		private var text:TextField;
		private var mc:MovieClip;
		private var grade:String;
		
		public static function create(w:BasicWindow, posX:int, posY:int, grade:String):ButtonBuy
		{
			var button:ButtonBuy = new ButtonBuy();
			button.grade = grade;
			button.init(w);
			button.x = posX;
			button.y = posY;
			
			return button;
		}
		
		public function ButtonBuy() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			button = new AntActor("ButtonBuy_mc");
			frameNormal = 1;
			frameHover = 2;
			
			clickSound = Kind.S_BUY;
			
			
			super.init(w);
			
			
			mc = new ButtonBuyText_mc();
			this.addChild(mc);
			mc.x = 10;
			
			text = mc.getChildByName("Text") as TextField;
			text.selectable = false;
		}
		
		public function updatePrice():void
		{
			var p:int = HeroParametrs.getGradePrice(grade);
			text.text = (p == -1)?"--":p.toString();
		}
		
		override protected function onClick():void
		{
			if (HeroParametrs.buyGrade(grade)) (window as ScreenMarket).updateParametrs();
		}			
	}
}