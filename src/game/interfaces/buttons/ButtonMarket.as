package src.game.interfaces.buttons 
{
	import src.framework.animation.AntActor;
	import src.game.core.Game;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ButtonMarket extends BasicButton
	{		
		public static function create(w:BasicWindow, posX:int, posY:int):ButtonMarket
		{
			var button:ButtonMarket = new ButtonMarket();
			button.init(w);
			button.x = posX;
			button.y = posY;
			
			return button;
		}
		
		public function ButtonMarket() 
		{
			
		}
		
		override public function init(w:BasicWindow):void
		{
			button = new AntActor("ButtonMarket_mc");
			frameNormal = 1;
			frameHover = 2;
			
			
			super.init(w);
		}
		
		override protected function onClick():void
		{
			Game.getInstance().windowsManager.switchScreen(Kind.SCREEN_MARKET);
		}			
	}
}