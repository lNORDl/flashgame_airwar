package src.game.interfaces.windows 
{
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import src.framework.animation.AntActor;
	import src.game.core.Game;
	import src.game.interfaces.buttons.BasicButton;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicWindow extends Sprite
	{
		public var id:int;
		public var rootLayer:Sprite;
		
		protected var background:AntActor;
		
		protected var elements:Array
		protected var buttons:Array
		
		protected var isHidden:Boolean;
		protected var isShown:Boolean;		
		public var isHide:Boolean;
		public var isShow:Boolean;
		
		public var parentWindow:BasicWindow;
		
		protected var game:Game;
		
		public var isActive:Boolean;
		
		protected var fadeTime:Number = -1;
		
		public function BasicWindow() 
		{
			game = Game.getInstance();
		}
		
		public function init(p:BasicWindow = null):void
		{
			elements = [];
			buttons = [];
			
			rootLayer = new Sprite();
			this.addChild(rootLayer);
			rootLayer.visible = false;			
			rootLayer.alpha = 0;
			
			isHidden = false;
			isHide = true;
			isShown = false;
			isShow = false;
			
			if (background !== null) rootLayer.addChild(background);
			
			parentWindow = p;
			
			if (parentWindow == null) game.rootLayer.addChild(this);
			else parentWindow.addWindow(this);
			
			game.windowsManager.addWindow(this);
			
			if(fadeTime == -1) fadeTime = 1;
		}
		
		public function addWindow(w:BasicWindow):void
		{
			elements.push(w);
			rootLayer.addChild(w);
		}
		
		public function addButton(b:BasicButton):void
		{
			buttons.push(b);
			rootLayer.addChild(b);
		}
		
		public function show():void
		{
			if (isHide && !isShown)
			{
				showBegin();
			}
		}
		
		public function hide():void
		{
			if (isShow && !isHidden)
			{
				hideBegin();
			}
		}
		
		protected function showBegin():void
		{
			isShown = true;
			isHide = false;
				
			rootLayer.visible = true;			
				
			TweenLite.to(rootLayer, fadeTime, { alpha:1, onComplete:showComplete } );
				
			//for (var i:int = 0; i < elements.length; i++) elements[i].show();
		}
		
		protected function hideBegin():void
		{
			isHidden = true;
			isShow = false;
			
			rootLayer.alpha = 1;
			TweenLite.to(rootLayer, fadeTime, { alpha:0, onComplete:hideComplete } );
			
			//for (var i:int = 0; i < elements.length; i++) elements[i].hide();
			
			isActive = false;
		}
		
		protected function showComplete():void
		{
			isShown = false;
			isShow = true;
			isHidden = false;
			isHide = false;
			
			isActive = true;
			
			for (var i:int = 0; i < buttons.length; i++) buttons[i].show();
		}
		
		protected function hideComplete():void
		{
			isHidden = false;
			isHide = true;
			isShown = false;
			isShow = false;
			
			rootLayer.visible = false;
			
			for (var i:int = 0; i < buttons.length; i++) buttons[i].hide();
		}
	}
}