package src.game.interfaces.windows.screens 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.framework.indecator.Indecator;
	import src.game.interfaces.buttons.ButtonBuy;
	import src.game.interfaces.buttons.ButtonCircle;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenMarket extends BasicWindow
	{
		private var buttonBuyHealth:ButtonBuy;
		private var buttonBuyRegen:ButtonBuy;
		private var buttonBuyBullets:ButtonBuy;
		private var buttonBuyPower1:ButtonBuy;
		private var buttonBuyPower2:ButtonBuy;
		
		private var buttonCircleGoMenu:ButtonCircle;
		private var buttonCircleGoNext:ButtonCircle;
		
		private var mc:MovieClip;
		private var coinText:TextField;
		
		private var indecatorHealth:Indecator;
		private var indecatorRegen:Indecator;
		private var indecatorBullets:Indecator;
		private var indecatorPower1:Indecator;
		private var indecatorPower2:Indecator;
		
		public function ScreenMarket() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{					
			background = new AntActor("Market_background_mc");
			id = Kind.SCREEN_MARKET;
			
			this.x = 250;
			this.y = 300;
			
			
			super.init(p);
			
			
			buttonBuyHealth = ButtonBuy.create(this, 115, -29, "health");
			buttonBuyRegen = ButtonBuy.create(this, 115, 43, "regen");
			buttonBuyBullets = ButtonBuy.create(this, 115, -98, "bullets");
			buttonBuyPower1 = ButtonBuy.create(this, 115, 113, "power1");
			buttonBuyPower2 = ButtonBuy.create(this, 115, 185, "power2");
			
			buttonCircleGoMenu = ButtonCircle.create(this, 73, 250, ButtonCircle.GO_MENU);
			buttonCircleGoNext = ButtonCircle.create(this, 123, 250, ButtonCircle.GO_NEXT);
			
			mc = new ScreenMarketCoinText_mc();
			rootLayer.addChild(mc);
			mc.x = 17;
			mc.y = -167;
			coinText = mc.getChildByName("CoinText") as TextField;
			
			indecatorHealth = new Indecator("ScreenMarketIconHeart_mc", 4, 3);
			rootLayer.addChild(indecatorHealth);
			indecatorHealth.x = -45; // -45
			indecatorHealth.y = -10; // -10
			
			indecatorRegen = new Indecator("ScreenMarketIconRect_mc", 4, 5);
			rootLayer.addChild(indecatorRegen);
			indecatorRegen.x = -48; // -48
			indecatorRegen.y = 60; // 60
			
			indecatorBullets = new Indecator("ScreenMarketIconBullet_mc", 4, 12);
			rootLayer.addChild(indecatorBullets);
			indecatorBullets.x = -45; // -45
			indecatorBullets.y = -80; // -81
			
			indecatorPower1 = new Indecator("ScreenMarketIconRect_mc", 3, 5);
			rootLayer.addChild(indecatorPower1);
			indecatorPower1.x = -33;
			indecatorPower1.y = 134;
			
			indecatorPower2 = new Indecator("ScreenMarketIconRect_mc", 3, 5);
			rootLayer.addChild(indecatorPower2);
			indecatorPower2.x = -33;
			indecatorPower2.y = 203;
		}
		
		override protected function showBegin():void
		{
			super.showBegin();
			
			
			updateParametrs();
			
			game.soundManager.playMainSound(Kind.S_MENU);
		}
		
		override protected function hideComplete():void
		{
			super.hideComplete();
			
			
			HeroParametrs.saveData();
		}
		
		public function updateParametrs():void
		{
			buttonBuyBullets.updatePrice();
			buttonBuyHealth.updatePrice();
			buttonBuyRegen.updatePrice();
			buttonBuyPower1.updatePrice();
			buttonBuyPower2.updatePrice();
			
			coinText.text = HeroParametrs.getCoins().toString();
			
			indecatorHealth.update(HeroParametrs.getGradeLvl("health"));
			indecatorRegen.update(HeroParametrs.getGradeLvl("regen"));
			indecatorBullets.update(HeroParametrs.getGradeLvl("bullets"));
			indecatorPower1.update(HeroParametrs.getGradeLvl("power1"));
			indecatorPower2.update(HeroParametrs.getGradeLvl("power2"));
		}
	}
}