package src.game.interfaces.windows.screens 
{
	import com.greensock.easing.Back;
	import com.greensock.easing.Bounce;
	import com.greensock.easing.Cubic;
	import com.greensock.easing.Sine;
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.buttons.ButtonCircle;
	import src.game.interfaces.buttons.ButtonMain;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenMainMenu extends BasicWindow
	{
		private var clouds:MovieClip;
		private var logo:MovieClip;
		//private var ground:MovieClip;
		private var plane1:AntActor;
		private var plane2:AntActor;
		private var plane1Step:int = 30;
		private var plane2Step:int = -50;
		
		private var buttonPlay:ButtonMain;
		private var buttonCredits:ButtonMain;
		private var buttonMoreGames:ButtonMain;
		
		private var buttonSound:ButtonCircle;
		
		public function ScreenMainMenu() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{
			background = new AntActor("MainMenu_background_mc");
			
			id = Kind.SCREEN_MAIN_MENU;
			
			
			super.init(p);
			
			
			clouds = new MainMenuClouds_mc();
			rootLayer.addChild(clouds);
			clouds.x = -265; // 256
			clouds.y = 530;
			
			//ground = new MainMenuGround_mc();
			//rootLayer.addChild(ground);
			//ground.x = 285;
			//ground.y = 711; // 571
			
			plane1 = new AntActor("MainMenuPlane1_mc");
			rootLayer.addChild(plane1);
			plane1.smoothing = true;
			plane1.x = 85;
			plane1.y = -5;
			plane1.rotation = 21;
			
			plane2 = new AntActor("MainMenuPlane2_mc");
			rootLayer.addChild(plane2);
			plane2.smoothing = true;
			plane2.x = 430;
			plane2.y = -15;
			plane2.rotation = -40;
			
			logo = new MainMenuLogo_mc();
			rootLayer.addChild(logo);
			logo.x = 270;
			logo.y = -100; // 125
			
			buttonPlay = ButtonMain.create(this, 250, -200, ButtonMain.PLAY);
			buttonCredits = ButtonMain.create(this, 250, -150, ButtonMain.CREDITS);
			buttonMoreGames = ButtonMain.create(this, 250, -100, ButtonMain.MORE_GAMES);
			
			buttonSound = ButtonCircle.create(this, 35, 35, ButtonCircle.SOUND);
		}	
		
		override protected function showBegin():void
		{
			super.showBegin();
			
			
			TweenLite.to(clouds, 1, { x:256, delay:0.15 } );
			TweenLite.to(logo, 0.9, { y:120 } );
			//TweenLite.to(ground, 0.8, { y:571 } );
			
			TweenLite.to(buttonPlay, 0.8, { y:257, delay: 0.7 });
			TweenLite.to(buttonCredits, 0.8, { y:355, delay: 0.6 });
			TweenLite.to(buttonMoreGames, 0.8, { y:451, delay: 0.5 } );
			
			game.soundManager.playMainSound(Kind.S_MENU);
			
			buttonSound.updateParametrs();
		}
		override protected function showComplete():void
		{
			super.showComplete();
			
			
			completePlane1();
			completePlane2();
		}
		
		override protected function hideBegin():void
		{
			super.hideBegin();
			
			
			TweenLite.to(clouds, 0.9, { x:-265} );
			TweenLite.to(logo, 1, { y:-100} );
			//TweenLite.to(ground, 0.8, { y:711 } );
			
			TweenLite.to(buttonPlay, 0.8, { y:-200 });
			TweenLite.to(buttonCredits, 0.8, { y:-150 });
			TweenLite.to(buttonMoreGames, 0.8, { y: -100 } );
			
			TweenLite.to(plane1, 3, { rotation:21});
			TweenLite.to(plane2, 3, { rotation:-40});
		}
		
		override protected function hideComplete():void
		{
			super.hideComplete();
			
			
			plane1Step = 30;
			plane2Step = -50;
		}
		
		private function rotateTo(mc:AntActor, r:int, t:Number, onC:Function):void
		{
			TweenLite.to(mc, t, { rotation:r, ease:Cubic.easeInOut, onComplete: onC});
		}
		
		private function completePlane1():void
		{
			if (isActive) 
			{
				rotateTo(plane1, plane1.rotation - plane1Step, 2.5, completePlane1);
				plane1Step *= -1;
			}
		}
		
		private function completePlane2():void
		{
			if (isActive) 
			{
				rotateTo(plane2, plane2.rotation - plane2Step, 3.5, completePlane2);
				plane2Step *= -1;
			}
		}
	}
}