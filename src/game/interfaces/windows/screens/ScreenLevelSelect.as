package src.game.interfaces.windows.screens 
{
	import src.framework.animation.AntActor;
	import src.game.interfaces.buttons.ButtonCircle;
	import src.game.interfaces.buttons.ButtonPlayLevel;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenLevelSelect extends BasicWindow
	{
		private var btns:Array;
		
		private var buttonCircleGoBack:ButtonCircle;
		private var buttonSound:ButtonCircle;
		
		public function ScreenLevelSelect() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{
			background = new AntActor("LevelSelect_background_mc");
			
			id = Kind.SCREEN_LEVEL_SELECT;
			
			this.x = 250;
			this.y = 300;
			
			
			super.init(p);
			
			
			btns = [];
			var pX:int = -160;
			var pY:int = -180;
			for (var i:int = 1; i < 10; i++)
			{
				var b:ButtonPlayLevel = ButtonPlayLevel.create(this, pX, pY, i);
				pX += 160;
				if (i % 3 == 0) 
				{
					pX = -160;
					pY += 120;
				}
				btns.push(b);
			}
			
			btns.push(ButtonPlayLevel.create(this, 0, pY, 10));
			
			buttonCircleGoBack = ButtonCircle.create(this, -215, 265, ButtonCircle.GO_BACK);
			buttonSound = ButtonCircle.create(this, -215, -265, ButtonCircle.SOUND);
		}	
		override protected function showBegin():void
		{
			super.showBegin();
			
			
			for (var i:int = 0; i < btns.length; i++) (btns[i] as ButtonPlayLevel).updateParametrs();
			
			game.soundManager.playMainSound(Kind.S_MENU);
			
			buttonSound.updateParametrs();
		}	
		
		override protected function showComplete():void
		{
			super.showComplete();
		}	
	}
}