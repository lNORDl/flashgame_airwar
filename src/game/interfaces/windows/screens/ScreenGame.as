package src.game.interfaces.windows.screens 
{
	import com.greensock.TweenLite;
	import flash.events.MouseEvent;
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.interfaces.windows.panels.PanelGameInfo;
	import src.game.interfaces.windows.panels.PanelLevelComplete;
	import src.game.interfaces.windows.panels.PanelLevelFailed;
	import src.game.interfaces.windows.panels.PanelPause;
	import src.game.levels.BasicLevel;
	import src.game.objects.hero.Hero;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ScreenGame extends BasicWindow
	{
		public static var lvl:int = -1;
		
		private var level:BasicLevel;
		private var universe:Universe = Universe.getInstance();
		
		private var panelGameInfo:PanelGameInfo;		
		private var panelLevelFailed:PanelLevelFailed;
		private var panelLevelComplete:PanelLevelComplete;
		private var panelPause:PanelPause;
		
		public function ScreenGame() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{						
			id = Kind.SCREEN_GAME;
			
			
			super.init(p);
			
			
			panelGameInfo = new PanelGameInfo();
			panelGameInfo.init(this);
			panelGameInfo.show();
			
			panelLevelFailed = new PanelLevelFailed();
			panelLevelFailed.init(this);
			
			panelLevelComplete = new PanelLevelComplete();
			panelLevelComplete.init(this);
			
			panelPause = new PanelPause();
			panelPause.init(this);
		}	
		
		override protected function showBegin():void
		{
			super.showBegin();
			
			
			TweenLite.to(universe.levelContainer, 1, { alpha:1 } );
			HeroParametrs.clearLevelInfo();
			universe.beginLevel();
			
			game.soundManager.playMainSound(Kind.S_GAME);
			panelGameInfo.updateSoundInfo();
			
			if (lvl == 1) panelGameInfo.pauseText.visible = true;
			else panelGameInfo.pauseText.visible = false;
		}
		
		override protected function hideBegin():void
		{
			super.hideBegin();
			
			
			TweenLite.to(universe.levelContainer, 1, { alpha:0 } );
			
			panelLevelFailed.hide();
			panelLevelComplete.hide();
		}
		
		override protected function hideComplete():void
		{
			super.hideComplete();
			
			
			universe.endLevel();
		}
	}
}