package src.game.interfaces.windows.panels 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.buttons.ButtonCircle;
	import src.game.interfaces.buttons.ButtonMarket;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class PanelLevelFailed extends BasicWindow
	{
		private var buttonCircleRestart:ButtonCircle;
		private var buttonCircleGoMenu:ButtonCircle;
		private var buttonMarket:ButtonMarket;
		
		private var mc:MovieClip;
		private var textCoins:TextField;
		private var textMedals:TextField;
		private var textEnemies:TextField;
		
		private var curCoins:int;
		private var curEnemies:int;
		private var curMedals:int;
		private var n:int = 0;
		private var n1:int = 0;
		private var n2:int = 0;
		
		public function PanelLevelFailed() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{
			background = new AntActor("PanelLevelFailed_mc");
			id = Kind.PANEL_LEVEL_FAILED;
			
			this.x = 250;
			this.y = 300;
			
			
			super.init(p);
			
			
			buttonCircleRestart = ButtonCircle.create(this, 112, 95, ButtonCircle.RESTART);
			buttonCircleGoMenu = ButtonCircle.create(this, -113, 95, ButtonCircle.GO_MENU);
			
			buttonMarket = ButtonMarket.create(this, -1, 95);
			
			mc = new PanelLevelEndText_mc();
			rootLayer.addChild(mc);
			mc.x = 15;
			mc.y = -64;
			textCoins = mc.getChildByName("TextCoins") as TextField;
			textMedals = mc.getChildByName("TextMedals") as TextField;
			textEnemies = mc.getChildByName("TextEnemies") as TextField;
		}
		
		override protected function showBegin():void
		{
			super.showBegin();
			
			
			curCoins = HeroParametrs.getCoins();
			curEnemies = HeroParametrs.enemies;
			curMedals = HeroParametrs.getMedals();
			
			HeroParametrs.changeCoins(HeroParametrs.enemies);
			HeroParametrs.changeCoins(HeroParametrs.getMedals() * 5);
			
			n = 0;
			n1 = 0;
			n2 = 0;
			
			Universe.getInstance().addFunction(updateCoinText);
			Universe.getInstance().addFunction(updateEnemiesText);
			Universe.getInstance().addFunction(updateMedalsText);
		}		
		
		private function updateCoinText():Boolean
		{
			if (curCoins <= HeroParametrs.getCoins())
			{
				if (n % 3 == 0)
				{
					textCoins.text = curCoins.toString();
					
					curCoins ++;
				}
				n ++;
				
				return false;
			}
			
			return true;
		}
		
		private function updateEnemiesText():Boolean
		{
			if (curEnemies >= 0 )
			{
				if (n1 > 30 && n1 % 5 == 0)
				{
					textEnemies.text = curEnemies.toString();
					
					curEnemies --;
				}
				n1++;
				
				return false;
			}
			
			return true;
		}
		
		private function updateMedalsText():Boolean
		{
			if (curMedals >= 0 )
			{
				if (n2 > 30 && n2 % 5 == 0)
				{
					textMedals.text = curMedals.toString();
					
					curMedals --;
				}
				n2++;
				
				return false;
			}
			
			return true;
		}
	}
}