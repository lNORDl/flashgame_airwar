package src.game.interfaces.windows.panels 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import src.framework.animation.AntActor;
	import src.game.interfaces.buttons.ButtonCircle;
	import src.game.interfaces.windows.BasicWindow;
	import src.game.objects.hero.SkillControl;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class PanelGameInfo extends BasicWindow
	{
		private var panel:MovieClip;
		
		private var textCoins:TextField;
		private var textMedals:TextField;
		
		private var buttonGoMenu:ButtonCircle;
		private var buttonSound:ButtonCircle;
		
		public var skillControl1:SkillControl;
		public var skillControl2:SkillControl;
		
		public var pauseText:MovieClip;
		
		public function PanelGameInfo() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{
			id = Kind.PANEL_GAME_INFO;
			
			
			super.init(p);
			
			
			panel = new PanelGameInfo_mc();
			rootLayer.addChild(panel);
			
			textCoins = panel.getChildByName("TextCoins") as TextField;
			textMedals = panel.getChildByName("TextMedals") as TextField;
			textCoins.text = "0";
			textMedals.text = "0";
			
			buttonGoMenu = ButtonCircle.create(this, 35, 35, ButtonCircle.GO_MENU);
			buttonSound = ButtonCircle.create(this, 85, 35, ButtonCircle.SOUND);
			
			skillControl1 = new SkillControl(40, 562, 1);
			rootLayer.addChild(skillControl1);	
			
			skillControl2 = new SkillControl(110, 562, 2);
			rootLayer.addChild(skillControl2);
			
			pauseText = panel.PauseText;
		}
		
		public function updCoinInfo(n:Number):void
		{
			textCoins.text = n.toString();
		}
		
		public function updMedalInfo(n:Number):void
		{
			textMedals.text = n.toString();
		}	
		
		public function updateSoundInfo():void
		{
			buttonSound.updateParametrs();
		}
	}
}