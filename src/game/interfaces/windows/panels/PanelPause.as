package src.game.interfaces.windows.panels 
{
	import src.framework.animation.AntActor;
	import src.game.core.Universe;
	import src.game.interfaces.buttons.ButtonPause;
	import src.game.interfaces.windows.BasicWindow;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class PanelPause extends BasicWindow
	{
		private var buttonPause:ButtonPause;
		
		public function PanelPause() 
		{
			
		}
		
		override public function init(p:BasicWindow = null):void
		{
			id = Kind.PANEL_PAUSE;
			background = new AntActor("PanelPause_mc");
			
			fadeTime = 0.2;
			
			this.x = 250;
			this.y = 300;
			
			
			super.init(p);
			
			
			buttonPause = new ButtonPause();
			buttonPause.init(this);
		}
		
		override protected function showComplete():void
		{
			super.showComplete();
			Universe.getInstance().switchPause();
		}
		
		override protected function hideComplete():void
		{
			super.hideComplete();
			Universe.getInstance().switchPause();
		}
	}
}