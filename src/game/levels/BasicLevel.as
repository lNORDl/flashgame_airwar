package src.game.levels 
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.utils.getQualifiedClassName;
	import nape.geom.Vec2;
	import src.game.core.Universe;
	import src.game.objects.enemies.BasicEnemy;
	import src.game.objects.enemies.Boss_1;
	import src.game.objects.enemies.Heinkel_1;
	import src.game.objects.enemies.Heinkel_2;
	import src.game.objects.enemies.Heinkel_3;
	import src.game.objects.others.BackgroundTile;
	import src.game.objects.others.Coin;
	import src.game.objects.others.Fog;
	import src.game.objects.others.Medal;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicLevel 
	{				
		public static var SUMMER:String = "SUMMER";
		public static var WINTER:String = "WINTER";
		public static var levelType:String;
		
		private static var storage:Array = [];
		
		private var universe:Universe;
		private var objects:Array;
		
		private var fogDefDelay:int = 200;
		private var fogCurDelay:int = 0;
		
		private var endPosition:int;
		
		public function BasicLevel() 
		{
			universe = Universe.getInstance();
		}
		
		public function init(name:String):void
		{
			for (var i:int = 0; i < storage.length; i++)
			{
				if (storage[i]["name"] == name)
				{
					objects = Kind.clone(storage[i]["objects"]) as Array;
					break;
				}
			}
			
			fogDefDelay = AntMath.randomRangeInt(200, 500);
			fogCurDelay = 0;
			
			levelType = objects["parametrs"]["type"];
			
			var x:String = (levelType == WINTER)?"Bg_11":"Bg_2";
			BackgroundTile.create(universe.position + 1, x);
			
			endPosition = objects["parametrs"]["l"];
		}
		
		public function update():void
		{
			var p:Number = universe.position;
			var mK:int = p / LevelCreator.SIZE;
			
			var r:Array = [];
			
			for (var k:int = 0; k <= mK; k++)
			{
				if (objects[k] is Array && objects[k].length > 0)
				{
					for (var i:int = 0; i < objects[k].length; i++)
					{
						if ((objects[k][i]["position"]  - LevelCreator.CREATE_SHIFT) <= p)
						{
							 //Координата созадния
							objects[k][i]["y"] = p - objects[k][i]["position"] + objects[k][i]["delay"];
							
							 //Создаем объект
							LevelCreator.createLevelObject(objects[k][i]);
							
							 //Добовляем на удаление
							r.push(objects[k][i]);
						}
					}
					
					for (var i:int = 0; i < r.length; i++)
					{
						objects[k].splice(objects[k].indexOf(r[i]), 1);
					}
					r = [];
				}
			}
			
			if (fogCurDelay == fogDefDelay)
			{
				fogCurDelay = 0;
				fogDefDelay = AntMath.randomRangeInt(500, 800);
				Fog.create(AntMath.randomRangeInt(100, 400), -250);
			}
			else fogCurDelay ++;
			
			//if (universe.position > endPosition + 300) trace(BasicEnemy.count);
			if (universe.position > endPosition + 300 && universe.position - BasicEnemy.last > 200 && BasicEnemy.count == 0 && !Boss_1.isEx)
			{
				universe.levelComplete();
			}
		}
		
		public function destroy():void
		{
			
		}	
		
		public static function addLevel(name:String, containers:Array):void
		{
			var a:Array = LevelCreator.getLevelObjects(containers);;
			var o:Array = new Array();
			o["name"] = name;
			o["objects"] = a;
			//trace(a["parametrs"]["start"], a["parametrs"]["type"]);
			storage.push(o);
		}
	}
}