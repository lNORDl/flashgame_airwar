package src.game.levels 
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.utils.getQualifiedClassName;
	import nape.geom.Vec2;
	import src.game.core.Universe;
	import src.game.objects.enemies.Heinkel_1;
	import src.game.objects.enemies.Heinkel_2;
	import src.game.objects.others.BackgroundTile;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicLevel 
	{		
		private const CREATE_SHIFT:int = 50;
		private const SIZE:int = 300;
		
		private var universe:Universe;
		private var objects:Array;
		
		private var conteiner:MovieClip;
		
		public function BasicLevel() 
		{
			universe = Universe.getInstance();
		}
		
		public function init(containers:Array):void
		{
			objects = [];
			
			for (var i:int = 0; i < 50; i++)
			{
				objects[i] = [];
			}
			
			
			for (var i:int = 0; i < containers.length; i++)
			{
				initContainer(containers[i]);
			}
		}
		
		private function initContainer(container:MovieClip):void
		{
			conteiner = container;
			var l:int = container.numChildren;
			for (var i:int = 0; i < l; i++)
			{
				var def:Array = new Array();
				var c:MovieClip = container.getChildAt(i) as MovieClip;
				if (c !== null)
				{
					def["position"] = Math.abs(c.y); 
					def["x"] = c.x;
					def["type"] = "NONE";
					def["delay"] = 0;
					if (c.delay !== undefined) def["delay"] = c.delay;
					
					def = getObjectDef(c, def);
					
					if (def["type"] !== "NONE")
					{
						var k:int = (def["position"] + def["delay"]  - CREATE_SHIFT) / SIZE;
						objects[k].push(def);
					}
				}
			}
		}
		
		public function update():void
		{
			var p:Number = universe.position;
			var mK:int = p / SIZE;
			
			var r:Array = [];
			
			for (var k:int = 0; k <= mK; k++)
			{
				for (var i:int = 0; i < objects[k].length; i++)
				{
					if (objects[k][i]["position"] + objects[k][i]["delay"] - CREATE_SHIFT <= p)
					{
						// Координата созадния
						objects[k][i]["y"] = p - objects[k][i]["position"] - CREATE_SHIFT;
						
						// Создаем объект
						createObject(objects[k][i]);
						
						// Добовляем на удаление
						r.push(objects[k][i]);
					}
				}
				
				for (var i:int = 0; i < r.length; i++)
				{
					objects[k].splice(objects[k].indexOf(r[i]), 1);
				}
				r = [];
			}
		}
		
		public function destroy():void
		{
			
		}	
		
		private function getObjectDef(mc:MovieClip, def:Array):Array
		{
			if (mc is Bg_1 || mc is Bg_2)			
			{
				def["type"] = Kind.T_BG_TILE;
				def["tile"] = getQualifiedClassName(mc);
			}
			else if (mc is Heinkel1_mc)
			{
				def["type"] = Kind.T_HEINKEL_1;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
				def["isShoot"] = mc.isShoot;
			}
			else if (mc is Heinkel2_mc)
			{
				def["type"] = Kind.T_HEINKEL_2;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			
			return def;			
		}
		
		private function createObject(def:Array):void
		{			
			switch(def["type"])
			{
				case Kind.T_BG_TILE:
					BackgroundTile.create(def["y"], def["tile"]);
				break;
				
				case Kind.T_HEINKEL_1:
					Heinkel_1.create(def["x"], def["y"], def["rot"], def["path"], def["speed"], def["isShoot"]);
				break;
				
				case Kind.T_HEINKEL_2:
					Heinkel_2.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
			}
		}
	}
}