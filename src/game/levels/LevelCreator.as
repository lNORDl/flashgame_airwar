package src.game.levels 
{
	import flash.display.MovieClip;
	import flash.utils.getQualifiedClassName;
	import src.game.objects.enemies.Boss_1;
	import src.game.objects.enemies.Colibri;
	import src.game.objects.enemies.Dornier;
	import src.game.objects.enemies.Heinkel_1;
	import src.game.objects.enemies.Heinkel_2;
	import src.game.objects.enemies.Heinkel_3;
	import src.game.objects.enemies.Heinkel_4;
	import src.game.objects.enemies.Heinkel_5;
	import src.game.objects.enemies.Horten;
	import src.game.objects.hero.Hero;
	import src.game.objects.others.BackgroundTile;
	import src.game.objects.others.BonusLive;
	import src.game.objects.others.BonusShield;
	import src.game.objects.others.Coin;
	import src.game.objects.others.Medal;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class LevelCreator 
	{
		public static const CREATE_SHIFT:int = 100;
		public static const SIZE:int = 300;
		
		public function LevelCreator() 
		{
			
		}
		
		public static function getLevelObjects(containers:Array):Array
		{
			var objects:Array = [];
			
			var max:int = 0;
			
			for (var i:int = 0; i < 50; i++)
			{
				objects[i] = [];
			}
			
			objects["parametrs"] = [];
			
			for (var j:int = 0; j < containers.length; j++)
			{
				var container:MovieClip = containers[j] as MovieClip;
				var parametrs:MovieClip = container.getChildByName("Parametrs") as MovieClip;
				objects["parametrs"]["type"] = parametrs.type;
				
				var l:int = container.numChildren;
				for (var i:int = 0; i < l; i++)
				{
					var def:Array = new Array();
					var c:MovieClip = container.getChildAt(i) as MovieClip;
					if (c !== null)
					{
						def["position"] = Math.abs(c.y) + parametrs.start; 
						def["x"] = c.x;
						def["type"] = "NONE";
						def["delay"] = 0;
						if (c.delay !== undefined) def["delay"] = c.delay;
						if (def["delay"] !== 0) def["delay"] += CREATE_SHIFT;
						
						def = getObjectDef(c, def);
						
						if (def["type"] !== "NONE")
						{
							var k:int = (def["position"] - CREATE_SHIFT) / SIZE;
							//trace(def.length);
							objects[k].push(def);
						}
						
						if (max < def["position"]) max = def["position"];
					}
				}
			}
			
			objects["parametrs"]["l"] = max;
			
			return objects;
		}
		
		
		private static function getObjectDef(mc:MovieClip, def:Array):Array
		{
			if (mc is Hero_mc)
			{
				def["type"] = Kind.T_HERO;
			}
			else if (mc is Bg_1 || mc is Bg_2 || mc is Bg_3 || mc is Bg_4)			
			{
				def["type"] = Kind.T_BG_TILE;
				def["tile"] = getQualifiedClassName(mc);
			}
			else if (mc is Heinkel1_mc)
			{
				def["type"] = Kind.T_HEINKEL_1;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
				def["isShoot"] = mc.isShoot;
			}
			else if (mc is Heinkel2_mc)
			{
				def["type"] = Kind.T_HEINKEL_2;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			else if (mc is Heinkel4_mc)
			{
				def["type"] = Kind.T_HEINKEL_4;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			else if (mc is Heinkel5_mc)
			{
				def["type"] = Kind.T_HEINKEL_5;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			else if (mc is Horten_mc)
			{
				def["type"] = Kind.T_HORTEN;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			else if (mc is Dornier_mc)
			{
				def["type"] = Kind.T_DORNIER;
				def["rot"] = mc.rotation;
				def["path"] = (mc.path == null)?"":mc.path;
				def["speed"] = (mc.speed == undefined)?0:mc.speed;
			}
			else if (mc is Heinkel3_mc)
			{
				def["type"] = Kind.T_HEINKEL_3;
				def["rot"] = mc.rotation;
				def["leftB"] = (mc.leftB == undefined)?0:mc.leftB;
				def["rightB"] = (mc.rightB == undefined)?0:mc.rightB;
				def["topB"] = (mc.topB == undefined)?0:mc.topB;
				def["botB"] = (mc.botB == undefined)?0:mc.botB;
			}
			else if (mc is Colibri_mc)
			{
				def["type"] = Kind.T_COLIBRI;
				def["rot"] = mc.rotation;
				def["leftB"] = (mc.leftB == undefined)?0:mc.leftB;
				def["rightB"] = (mc.rightB == undefined)?0:mc.rightB;
				def["topB"] = (mc.topB == undefined)?0:mc.topB;
				def["botB"] = (mc.botB == undefined)?0:mc.botB;
			}
			else if (mc is Coin_mc)
			{
				def["type"] = Kind.T_COIN;
			}
			else if (mc is Medal_mc)
			{
				def["type"] = Kind.T_MEDAL;
			}
			else if (mc is BonusLive_mc)
			{
				def["type"] = Kind.T_LIVE;
			}
			else if (mc is BonusShield_mc)
			{
				def["type"] = Kind.T_SHIELD;
			}
			else if (mc is BossCenter_mc)
			{
				def["type"] = Kind.T_BOSS_1;
			}
			
			return def;			
		}
		
		public static function createLevelObject(def:Array):void
		{			
			switch(def["type"])
			{
				case Kind.T_HERO:
					Hero.create(def["x"], def["y"]);
				break;
				
				case Kind.T_BG_TILE:
					BackgroundTile.create(def["y"], def["tile"]);
				break;
				
				case Kind.T_HEINKEL_1:
					Heinkel_1.create(def["x"], def["y"], def["rot"], def["path"], def["speed"], def["isShoot"]);
				break;
				
				case Kind.T_HEINKEL_2:
					Heinkel_2.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
				
				case Kind.T_HORTEN:
					Horten.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
				
				case Kind.T_DORNIER:
					Dornier.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
				
				case Kind.T_HEINKEL_3:
					Heinkel_3.create(def["x"], def["y"], def["leftB"], def["rightB"], def["topB"], def["botB"]);
				break;
				
				case Kind.T_COLIBRI:
					Colibri.create(def["x"], def["y"], def["leftB"], def["rightB"], def["topB"], def["botB"]);
				break;
				
				case Kind.T_HEINKEL_4:
					Heinkel_4.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
				
				case Kind.T_HEINKEL_5:
					Heinkel_5.create(def["x"], def["y"], def["rot"], def["path"], def["speed"]);
				break;
				
				case Kind.T_BOSS_1:
					Boss_1.create();
				break;
				
				case Kind.T_COIN:
					Coin.create(def["x"], def["y"]);
				break;
				
				case Kind.T_MEDAL:
					Medal.create(def["x"], def["y"]);
				break;
				
				case Kind.T_LIVE:
					BonusLive.create(def["x"], def["y"]);
				break;
				
				case Kind.T_SHIELD:
					BonusShield.create(def["x"], def["y"]);
				break;
			}
		}
		
	}

}