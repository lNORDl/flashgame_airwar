package src.game.objects.hero 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import src.game.core.Universe;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class SkillControl extends Sprite
	{
		private var mc:MovieClip;
		private var m:MovieClip;
		private var t:TextField;
		private var l:MovieClip;
		
		private var s:String;
		
		private var delayCur:int = -1;
		private var delayMax:int = -1;
		private var size:int = 59;
		
		public function SkillControl(posX:int, posY:int, type:int, allow:Boolean = true) 
		{
			this.x = posX;
			this.y = posY;
			
			mc = new Skill_mc();
			this.addChild(mc);
			m = mc.Mask;
			t = mc.Text;
			l = mc.Lock;
			
			m.width = 0;
			
			s = type == 1?"Z":"X";
			t.text = s;
			
			if (allow) m.width = size;
			
			Universe.getInstance().addFunction(update);
		}
		
		public function reset(d:int):void
		{
			delayMax = d;
			delayCur = 0;
		}
		
		private function update():Boolean
		{
			if (delayCur <= delayMax)
			{
				//trace("XXXX");
				m.width = (delayCur / delayMax) * size;
				delayCur ++;
			}
			
			
			return false;
		}
		
		public function lock(b:Boolean):void
		{
			if (b)
			{
				l.visible = true;
				t.text = "";
			}
			else
			{
				l.visible = false;
				t.text = s;
			}
		}		
	}

}