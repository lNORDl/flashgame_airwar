package src.game.objects.hero 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class HealthBar extends BasicObject
	{
		private var maxHealth:int;
		private var hero:Hero;
		
		private var hearts:Array;
		
		private var lastHealth:int;
		
		public static function create(hero:Hero, maxHealth:int):HealthBar
		{
			var object:HealthBar = new HealthBar();
			object.hero = hero;
			object.maxHealth = maxHealth;
			object.init(hero.x, hero.y, 0);
			
			return object;
		}
		
		public function HealthBar() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.heroContainer;
			
			var hS:int = 3;
			var hW:int = 14;
			
			lastHealth = maxHealth * 2;
			
			var w:int = maxHealth * hW + ((maxHealth - 1) * hS);
			var s:Number = hW + hS;
			var p:Number = -((w - hW) / 2);
			hearts = [];
			for (var i:int = 0; i < maxHealth; i++)
			{
				var heart:AntActor = new AntActor("Heart_mc");
				this.addChild(heart);
				heart.x = p;
				p += s;
				hearts.push(heart);
			}
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update():void
		{
			super.update();
			
			
			this.x = hero.x;
			this.y = hero.y + 45;
		}
		
		public function updateHealth(health:int):void
		{
			if (lastHealth !== health)
			{
				var n:int = health / 2;
				for (var i:int = 0; i < hearts.length; i++)
				{
					hearts[i].gotoAndStop(i < n?1:3);
				}
				if (health > n * 2)
				{
					hearts[n].gotoAndStop(2);
				}
				lastHealth = health;
			}
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			super.checkOnDestroy(false);
		}
		
		override public function destroy():void
		{	
			if (isExist)
			{
				hero = null;			
				for (var i:int = 0; i < hearts.length; i++)
				{
					hearts[i].free();
					hearts[i] = null;
				}
				hearts = null;
			}
			
			
			super.destroy();
		}			
	}
}