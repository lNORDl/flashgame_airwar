package src.game.objects.hero 
{
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.framework.id.IdFilter;
	import src.game.core.Game;
	import src.game.core.Universe;
	import src.game.interfaces.windows.panels.PanelGameInfo;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletH100;
	import src.game.objects.bullets.BulletH200;
	import src.game.objects.effects.ExplosionE200;
	import src.game.objects.effects.HeroSpot;
	import src.game.objects.others.IBonus;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author NORD
	 */
	public class Hero extends BasicObject implements IDestoyable
	{
		public static var grades:Array;
		
		private var maxHealth:int;		
		private var speed:Number;	
		private var regenDelay:int;
		private var shootDelay:int;
		private var shootCount:int;
		private var power1Delay:int;
		private var power2Delay:int;
		private var power1N:int;
		private var power2N:int;
		private var power2P:int;
		private var power2C:int;
		private var power2K:int;
		private var isPower2:Boolean;
		
		private var enemyContactDef:Array;
		private var bonusContactDef:Array;
		
		private var shootTimer:FrameTimer;
		private var regenTimer:FrameTimer;
		private var power1Timer:FrameTimer;
		private var power2Timer:FrameTimer;
		private var power2ShootTimer:FrameTimer;
		
		private var health:HealthSystem;		
		private var healthBar:HealthBar;
		
		private var isShield:Boolean;
		private var shieldTimer:FrameTimer;
		private var shield:AntActor;
		
		private var enemyTimer:FrameTimer;
		
		private var enemies:Array;
		
		public static function create(posX:Number, posY:Number):Hero
		{
			var object:Hero = new Hero();
			object.init(posX, posY, 270);
			
			return object;
		}
		
		public function Hero() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.heroContainer;
			
			actor = new AntActor("Hero_mc");
			
			id.addTags([Kind.GROUP_A]);
			
			grades = HeroParametrs.getGrades();
			
			initParametrs();
			
			super.init(posX, posY, rot);
			
			
			health = new HealthSystem(this, maxHealth);
			healthBar = HealthBar.create(this, maxHealth / 2);
			
			shield = new AntActor("Shield_mc");
			this.addChild(shield);
			shield.visible = false;
			
			enemyContactDef = [body, [Kind.GROUP_B], Kind.OR, onEnemyContact];
			universe.addCheckContact(enemyContactDef);
			
			bonusContactDef = [body, [Kind.T_BONUS], Kind.OR, onBonusContact];
			universe.addCheckContact(bonusContactDef);
			
			universe.gStage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			
			universe.hero = this;
			
			(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl1.reset(power1Delay);
			(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl2.reset(power2Delay);
		}
		
		private function keyDown(e:KeyboardEvent):void 
		{
			if (e.keyCode == 32 || e.keyCode == 80)
			{
				universe.switchPause();
			}
			
			if (e.keyCode == 90)
			{
				if (power1Timer !== null && power1Timer.isTick)
				{
					(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl1.reset(power1Delay);
					
					power1Timer.start();
					
					var n:int = power1N;
					var a:Number = 0;
					for (var i:int = 0; i < n; i++)
					{
						BulletH100.create(position.x, position.y, a);
						a += 360 / n;
					}
				}
			}
			
			if (e.keyCode == 88)
			{
				if (power2Timer !== null && power2Timer.isTick)
				{
					(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl2.reset(power2Delay);
					power2Timer.start();
					enemies = universe.objects.getObjectsByTagOR([Kind.GROUP_B]);
					isPower2 = true;
				}
			}
		}
		
		private function initParametrs():void
		{
			switch(grades["health"])
			{
				case 1: maxHealth = 6; break;
				case 2: maxHealth = 8; break;
				case 3: maxHealth = 10; break;
				case 4: maxHealth = 12; break;
			}
			
			switch(grades["regen"])
			{
				case 1: regenDelay = 420; break;
				case 2: regenDelay = 360; break;
				case 3: regenDelay = 300; break;
				case 4: regenDelay = 240; break;
			}
			
			switch(grades["power1"])
			{
				case 1: power1Delay = 500; power1N = 40; break;
				case 2: power1Delay = 400; power1N = 60; break;
				case 3: power1Delay = 300; power1N = 80; break;
			}	
			
			switch(grades["power2"])
			{
				case 1: power2Delay = 650; power2N = 2; break;
				case 2: power2Delay = 550; power2N = 4; break;
				case 3: power2Delay = 450; power2N = 6; break;
			}
			
			switch(grades["bullets"])
			{
				case 1:
					shootCount = 1;
					shootDelay = 17;
					break;
				case 2: 
					shootCount = 2; 
					shootDelay = 17;
					break;
				case 3:
					shootCount = 3; 
					shootDelay = 22;
					break;
				case 4:
					shootCount = 4; 
					shootDelay = 22;
					break;
			}
			
			speed = 4.5;		
			
			shootTimer = new FrameTimer(shootDelay);
			regenTimer = new FrameTimer(regenDelay, 0);
			
			enemyTimer = new FrameTimer(50, 50);
			
			shieldTimer = new FrameTimer(180, 0);
			shieldTimer.stop();
			
			if (grades["power1"] > 0)
			{
				(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl1.lock(false);
				power1Timer = new FrameTimer(power1Delay, 0);
			}
			else (Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl1.lock(true);
			if (grades["power2"] > 0)
			{
				(Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl2.lock(false);
				power2Timer = new FrameTimer(power2Delay, 0);
				power2ShootTimer = new FrameTimer(10, 10);
				power2ShootTimer.stop();
				power2P = -20;
				power2C = power2N;
				power2K = 1;
			}
			else (Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo).skillControl2.lock(true);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(30, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			var d:Number = AntMath.distance(this.x, this.y, universe.gStage.mouseX, universe.gStage.mouseY);
			var vec:Vec2 = new Vec2(0, 0);
			if (d > speed)
			{
				vec = AntMath.getMoveVector(speed*(d/50), AntMath.angle(this.x, this.y, universe.gStage.mouseX, universe.gStage.mouseY) * AntMath.TO_DEGREES);
				body.position.x += vec.x * universe.xt;
				body.position.y += vec.y * universe.xt;
				//if (body.position.y < 200) body.position.y = 200;
				
				if (vec.x > 1.5) actor.gotoAndStop(2);
				else if (vec.x < -1.5) actor.gotoAndStop(3);
				else actor.gotoAndStop(1);
			}
			
			
			if (shootTimer.isTick && !isPower2) 
			{
				var k:Number = vec.x * universe.xt * 2.5;
				var w:Number = shootCount * 10 + (shootCount - 1) * 5;
				var p:Number = -(w - 10) / 2;
				for (var i:int = 0; i < shootCount; i++)
				{					
					BulletH100.create(position.x + p + k, position.y-5, degAngle);
					p += 15;
				}				
			}
			
			if (regenTimer.isTick)
			{
				health.addHealth(1);
			}
			
			if (power1Timer !== null && power1Timer.isTick) power1Timer.stop();
			if (power2Timer !== null && power2Timer.isTick) power2Timer.stop();
			
			if (isPower2)
			{
				power2ShootTimer.start();
				if (power2ShootTimer.isTick && power2C > 0)
				{
					power2C --;
					var n:int = AntMath.randomRangeInt(0, enemies.length - 1);
					BulletH200.create(position.x+10, position.y, 0, enemies[n]);
					n = AntMath.randomRangeInt(0, enemies.length - 1);
					BulletH200.create(position.x-10, position.y, 180, enemies[n]);
					
					if (power2C == 0)
					{
						power2ShootTimer.stop();
						isPower2 = false;
						power2C = power2N;
					}
				}
			}
			
			healthBar.updateHealth(health.curHealth);
			
			if (enemyTimer.isTick) enemyTimer.stop();
			
			if (isShield && shieldTimer.isTick) 
			{
				isShield = false;
				health.isImmortal = false;
				shieldTimer.reset();
				shieldTimer.stop();
				shield.visible = false;
			}
		}
		
		private function onEnemyContact(object:BasicObject):void
		{
			if (enemyTimer.isTick)
			{
				enemyTimer.reset(0);
				enemyTimer.start();
				
				(object as IDestoyable).getHealthSystem().addDamage(20);
				if(!isShield) health.addDamage(2);
			}
		}
		
		public function onDamage():void
		{
			HeroSpot.create();
		}
		
		private function onBonusContact(object:BasicObject):void
		{
			var bonus:IBonus = object as IBonus;
			if (bonus !== null) bonus.activate(this);
		}
		
		public function activateShield():void
		{
			isShield = true;
			health.isImmortal = true;
			shieldTimer.reset(0);
			shieldTimer.start();
			shield.visible = true;
		}
		
		public function getHealthSystem():HealthSystem
		{
			return health;
		}
		
		public function liquidate():void
		{
			ExplosionE200.create(position.x, position.y);
			destroy();
			Universe.getInstance().levelFailed();
		}
		
		override public function destroy():void
		{		
			universe.removeCheckContact(enemyContactDef);
			universe.removeCheckContact(bonusContactDef);
			
			if(healthBar.isExist) healthBar.destroy();
			
			shootTimer.destroy();
			regenTimer.destroy();
			if(power1Timer !== null) power1Timer.destroy();
			if(power2Timer !== null) power2Timer.destroy();
			shieldTimer.destroy();
			
			shootTimer = null;
			regenTimer = null;
			power1Timer = null;
			power2Timer = null;
			shieldTimer = null;
			
			universe.gStage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			
			grades = null;
			
			shield.free();
			shield = null;
			
			health.destroy();
			health = null;
			
			enemies = null;
			
			
			super.destroy();	
		}
	}
}