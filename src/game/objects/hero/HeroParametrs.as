package src.game.objects.hero 
{
	import flash.net.SharedObject;
	import src.game.core.Game;
	import src.game.interfaces.windows.panels.PanelGameInfo;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class HeroParametrs 
	{		
		public static const LVL_COUNT:int = 10;
		
		private static var medals:int = 0;
		private static var coins:int = 0;
		private static var grades:Array;
		
		public static var curLvl:int = 1;
		public static var maxLvl:int = 1;
		
		public static var enemies:int = 0;
		
		private static var panelGameInfo:PanelGameInfo;
		
		public static function changeCoins(n:int):void
		{
			coins += n;
			
			panelGameInfo.updCoinInfo(coins);
		}
		
		public static function getCoins():int
		{
			return coins;
		}
		
		public static function changeMedals(n:int):void
		{
			medals += n;
			
			panelGameInfo.updMedalInfo(medals);	
		}
		
		public static function getMedals():int
		{
			return medals;
		}
		
		public static function getMaxLevel():int
		{
			return maxLvl;
		}
		
		public static function getCurrentLevel():int
		{
			return curLvl;
		}
		
		public static function clearLevelInfo():void
		{
			enemies = 0;
			changeMedals(-medals);
		}
		
		public static function init():void
		{
			panelGameInfo = (Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_GAME_INFO) as PanelGameInfo);
			
			if (isData() && false)
			{
				loadData();
			}
			else
			{
				curLvl = 1;
				maxLvl = 10;
				
				changeMedals(0);
				changeCoins(0);
				
				grades = [];
				
				grades["bullets"] = [];
				grades["bullets"]["lvl"] = 1;
				grades["bullets"]["price"] = [70, 70, 450, 800];
				
				grades["health"] = [];
				grades["health"]["lvl"] = 1;
				grades["health"]["price"] = [100, 120, 300, 450];
				
				grades["regen"] = [];
				grades["regen"]["lvl"] = 1;
				grades["regen"]["price"] = [5, 20, 80, 120];
				
				grades["power1"] = [];
				grades["power1"]["lvl"] = 0;
				grades["power1"]["price"] = [100, 250, 500];
				
				grades["power2"] = [];
				grades["power2"]["lvl"] = 0;
				grades["power2"]["price"] = [100, 250, 500];
				
				saveData();
			}
			
			//trace(maxLvl, curLvl);
		}
		
		public static function getGradeLvl(name:String):int
		{
			return grades[name]["lvl"];
		}
		
		public static function getGradePrice(name:String):int
		{
			var l:int = grades[name]["price"].length;
			if (l == grades[name]["lvl"]) return -1;
			return grades[name]["price"][grades[name]["lvl"]];
		}
		
		public static function buyGrade(name:String):Boolean
		{
			var l:int = grades[name]["price"].length;
			if (l == grades[name]["lvl"]) return false;
			if (coins >= grades[name]["price"][grades[name]["lvl"]])
			{
				changeCoins(-grades[name]["price"][grades[name]["lvl"]]);
				grades[name]["lvl"] ++;
				
				return true;
			}
			return false;
		}
		
		public static function getGrades():Array
		{
			var a:Array = [];
			a["health"] =  grades["health"]["lvl"];
			a["regen"] =   grades["regen"]["lvl"];
			a["bullets"] = grades["bullets"]["lvl"];
			a["power1"] =  grades["power1"]["lvl"];
			a["power2"] =  grades["power2"]["lvl"];
			
			return a;
		}	
		
		private static function isData():Boolean
		{
			var s:SharedObject = SharedObject.getLocal("AirWarData"); 
			if (s.data.isSave == undefined) return false;
			return true;
		}
		
		public static function saveData():void
		{
			var s:SharedObject = SharedObject.getLocal("AirWarData"); 
			s.data.isSave = true;
			s.data.grades = grades;
			s.data.coins = coins;
			s.data.maxLvl = maxLvl;
			s.flush();			
		}
		
		public static function loadData():void
		{
			var s:SharedObject = SharedObject.getLocal("AirWarData"); 
			grades = s.data.grades as Array;
			coins = 0;
			changeCoins(s.data.coins);
			maxLvl = s.data.maxLvl;
		}
	}
}