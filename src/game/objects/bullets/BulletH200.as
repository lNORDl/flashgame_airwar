package src.game.objects.bullets 
{
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.enemies.Heinkel_1;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BulletH200 extends BasicObject
	{
		private var speed:Number;
		private var damage:Number;
		private var contact:Array;
		private var target:BasicObject;
		private var targetPoint:Vec2;
		
		public static function create(posX:Number, posY:Number, rot:Number, target:BasicObject):BulletH200
		{
			var object:BulletH200 = new BulletH200();
			object.target = target;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function BulletH200() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.bulletsContainer;
			
			actor = new AntActor("BulletH200_mc");
			actor.play();
			
			speed = 6;
			damage = 5;
			
			targetPoint = new Vec2();
			
			
			super.init(posX, posY, rot);
			
			
			contact = [body, [Kind.GROUP_B], Kind.OR, onContact];
			universe.addCheckContact(contact);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(5, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (target == null)
			{
				targetPoint.x = position.x;
				targetPoint.y = position.y - 20;
			}
			else
			{
				targetPoint.x = target.position.x;
				targetPoint.y = target.position.y;
				
				if (!target.isExist) target = null;
			}
			
			var r:Number = AntMath.getRotAngle(position.x, position.y, targetPoint.x, targetPoint.y, degAngle, 3);
			body.rotation += r * AntMath.TO_RADIANS;
			
			var vec:Vec2 = AntMath.getMoveVector(speed, degAngle);
			body.position.x += vec.x * universe.xt;
			body.position.y += vec.y * universe.xt;
		}
		
		private function onContact(object:BasicObject):void
		{
			var h:IDestoyable = object as IDestoyable;
			if (h !== null && object.y > 0) 
			{
				h.getHealthSystem().addDamage(damage);
				destroy();
			}
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (!universe.isInBounds(position.x, position.y)) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}
		
		override public function destroy():void
		{			
			universe.removeCheckContact(contact);
			
			
			super.destroy();
		}		
	}
}