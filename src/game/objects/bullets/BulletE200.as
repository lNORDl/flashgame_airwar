package src.game.objects.bullets 
{
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.enemies.Heinkel_1;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BulletE200 extends BasicObject
	{
		private var speed:Number;
		private var damage:Number;
		private var contact:Array;
		private var vec:Vec2;
		
		private var isC:Boolean;
		
		public static function create(posX:Number, posY:Number, rot:Number, speed:Number, c:Boolean = false):BulletE200
		{
			var object:BulletE200 = new BulletE200();
			object.speed = speed;
			object.isC = c;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function BulletE200() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = (isC)?universe.overgroundContainer:universe.bulletsContainer;
			
			actor = new AntActor("BulletE200_mc");
			
			damage = 2;
			
			
			super.init(posX, posY, rot);
			
			
			contact = [body, [Kind.GROUP_A], Kind.OR, onContact];
			universe.addCheckContact(contact);
			
			vec = AntMath.getMoveVector(speed, degAngle);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(5, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			body.position.x += vec.x * universe.xt;
			body.position.y += vec.y * universe.xt;
		}
		
		private function onContact(object:BasicObject):void
		{
			var h:IDestoyable = object as IDestoyable;
			if (h !== null) 
			{
				h.getHealthSystem().addDamage(damage);
			}
			destroy();
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (!universe.isInBounds(position.x, position.y)) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}
		
		override public function destroy():void
		{			
			universe.removeCheckContact(contact);
			
			
			super.destroy();
		}			
	}
}