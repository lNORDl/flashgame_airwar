package src.game.objects.bullets 
{
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.effects.ExplosionE50;
	import src.game.objects.enemies.Heinkel_1;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BulletE300 extends BasicObject
	{
		private var speed:Number;
		private var rS:Number;
		private var damage:Number;
		private var contact:Array;
		
		private var liveTimer:FrameTimer;
		
		public static function create(posX:Number, posY:Number, rot:Number, speed:Number, rS:Number):BulletE300
		{
			var object:BulletE300 = new BulletE300();
			object.speed = speed;
			object.rS = rS;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function BulletE300() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.bulletsContainer;
			
			actor = new AntActor("BulletE300_mc");
			
			damage = 5;
			
			
			super.init(posX, posY, rot);
			
			
			contact = [body, [Kind.GROUP_A], Kind.OR, onContact];
			universe.addCheckContact(contact);
			
			liveTimer = new FrameTimer(170+AntMath.randomRangeInt(0, 80), 0);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(5, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			var a:Number = 0;
			if (universe.hero.isExist) a = AntMath.getRotAngle(position.x, position.y, universe.hero.position.x, universe.hero.position.y, degAngle, rS);
			body.rotation += a * AntMath.TO_RADIANS;
			
			var vec:Vec2 = AntMath.getMoveVector(speed, degAngle);
			body.position.x += vec.x * universe.xt;
			body.position.y += vec.y * universe.xt;
			
			if (liveTimer.isTick) liquidate();
		}
		
		private function onContact(object:BasicObject):void
		{
			var h:IDestoyable = object as IDestoyable;
			if (h !== null) h.getHealthSystem().addDamage(damage);
			liquidate();
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (!universe.isInBounds(position.x, position.y)) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}
		
		public function liquidate():void
		{
			ExplosionE50.create(position.x, position.y);
			destroy();
		}
		
		override public function destroy():void
		{			
			universe.removeCheckContact(contact);
			liveTimer.destroy();
			liveTimer = null;
			
			
			super.destroy();
		}		
	}
}