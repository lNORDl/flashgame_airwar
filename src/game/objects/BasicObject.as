package src.game.objects 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import src.framework.animation.AntActor;
	import src.framework.id.Id;
	import src.game.core.Universe;
	import src.utils.AntMath;
	import src.utils.BehaviorsStorage;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicObject extends Sprite
	{				
		// PUBLIC
		public var isActive:Boolean;
		public var isExist:Boolean;
		
		public var degAngle:Number;
		public var radAngle:Number;
		public var position:Vec2;		
		
		public var container:MovieClip;
		public var actor:AntActor;
		public var behaviors:BehaviorsStorage;		
		public var body:Body;
		public var id:Id; 		
		
		protected var universe:Universe;
		protected var isActorSmoothing:Boolean = true;
		
		public function BasicObject() 
		{
			universe = Universe.getInstance();
			
			isExist = true;
			isActive = true;			
			
			id = new Id([]);
			
			position = new Vec2(0, 0);
		}
		
		public function init(posX:Number, posY:Number, rot:Number):void
		{					
			if (container == null ) trace(this + " dont have container!");
			else container.addChild(this);
			
			this.x = posX;
			this.y = posY;
			this.rotation = rot;
			position.x = posX;
			position.y = posY;
			degAngle = rot;
			radAngle = degAngle * AntMath.TO_RADIANS;
			
			if (actor !== null) 
			{
				this.addChild(actor);
				actor.smoothing = isActorSmoothing;
			}	
			
			initBody();
			
			behaviors = new BehaviorsStorage();
			
			universe.objects.add(this);
		}
		
		protected function initBody(data:* = null):void
		{
			if (body !== null)
			{
				//body.align();				
				body.userData.Object = data;	
				universe.space.bodies.add(body);
			}
			
			updatePosition();
		}		
		
		public function update():void
		{	
			if (isExist)
			{
				behaviors.update();
				updatePosition();
			}
		}
		
		public function afterUpdate():void
		{
			if(isExist) checkOnDestroy();
		}
		
		private function updatePosition():void
		{
			if (body !== null)
			{
				position.x = body.position.x;
				position.y = body.position.y;
				radAngle = body.rotation;
				degAngle = radAngle * AntMath.TO_DEGREES;
				
				this.x = position.x;
				this.y = position.y;
				this.rotation = degAngle;
			}	
			else
			{
				position.x = this.x;
				position.y = this.y;
				degAngle = this.rotation;
				radAngle = degAngle * AntMath.TO_RADIANS;
			}
		}
		
		protected function checkOnDestroy(c:Boolean = false):void
		{
			if (c == true)
			{
				destroy();
			}
		}		
		
		public function destroy():void
		{
			if (isExist)
			{
				behaviors.destroy();
				behaviors = null;
				
				if (container !== null && container.contains(this)) container.removeChild(this);
				if (actor !== null) actor.free();
				
				if (body !== null)
				{
					body.userData.Object = null;
					universe.space.bodies.remove(body);
				}
				
				container = null;
				actor = null;
				body = null;
				
				universe.objects.remove(this);
				universe = null;
				
				isExist = false;	
				isActive = false;
			}
		}
	}
}