package src.game.objects.enemies 
{
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import nape.geom.Vec2;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.core.Game;
	import src.game.objects.BasicObject;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BasicEnemy extends BasicObject implements IDestoyable
	{
		public static var count:int = 0;
		public static var last:int = 0;
		
		protected var isUsePath:Boolean;
		protected var path:Array;
		protected var pathDef:Array;
		protected var pathName:String;
		
		protected var isUseBounds:Boolean;
		protected var leftB:int;
		protected var rightB:int;
		protected var topB:int;
		protected var botB:int;
		protected var time:int;
		protected var waitOnPointTimer:FrameTimer;
		
		protected var targetPoint:Vec2;
		
		protected var health:HealthSystem;
		protected var maxHealth:int = 1;
		protected var speed:Number = 1;
		protected var rotationSpeed:Number = 1;
		
		protected var isShoot:Boolean;
		protected var shootTimer:FrameTimer;
		protected var reload:int;
		
		protected var liquidateSound:int = -1;
		
		protected var white:MovieClip;
		
		public function BasicEnemy() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.centergroundContainer;			
			id.addTags([Kind.GROUP_B]);
			
			
			super.init(posX, posY, rot);
			
			
			count ++;
			
			health = new HealthSystem(this, maxHealth);
			
			if(isShoot) shootTimer = new FrameTimer(reload, AntMath.randomRangeInt(reload / 2, reload));
			
			targetPoint = null;
			
			if (white !== null)
			{
				this.addChild(white);
				white.alpha = 0;
				white.cacheAsBitmap = true;
			}
			
			if (liquidateSound == -1) liquidateSound = Kind.S_EXPLOSION_E100;
			
			if (isUsePath)
			{
				var p:Array = universe.getPath(pathName);	
				
				if (p.length !== 0)
				{
					pathDef = p[0].concat();
					path = [];
					for (var j:int = 0; j < p[1].length; j++)
					{
						path[j] = new Vec2(p[1][j].x, p[1][j].y);
					}
					
					if (pathDef[1] == "local")
					{
						for (var i:int = 0; i < path.length; i++)
						{
							var dot:Point = new Point(path[i].x, path[i].y);
							var d:Point = this.localToGlobal(dot);
							path[i].x = d.x;
							path[i].y = d.y;
							//Kind.dot(d.x, d.y);
							//trace(path[i], p[1][i]);
						}
					}
				}
				else path = [];
			}
			
			if (isUseBounds)
			{
				waitOnPointTimer = new FrameTimer(time, 0);
				waitOnPointTimer.stop();
				
				targetPoint = new Vec2(AntMath.randomRangeInt(leftB, rightB), AntMath.randomRangeInt(topB, botB));
			}
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (isUsePath)
			{
				while (path !== null)
				{
					if (path.length !== 0)
					{
						var p:Vec2 = path[0];
						var d:Number = AntMath.distance(this.x, this.y, p.x, p.y);
						if(d <= 15)
						{
							path.splice(0, 1);
							continue;
						}
						
						targetPoint = p;
						break;
					}
					else
					{
						targetPoint = null;
						break;
					}
				}
				
				//trace(targetPoint);
			}
			
			if (isUseBounds)
			{
				var isNeedChange:Boolean = false;
				if (targetPoint !== null)
				{
					var d:Number = AntMath.distance(position.x, position.y, targetPoint.x, targetPoint.y);
					if (d < 10)
					{
						waitOnPointTimer.start();
						if (waitOnPointTimer.isTick)
						{
							waitOnPointTimer.reset();
							waitOnPointTimer.stop();
							isNeedChange = true;
						}
					}
				}
				if (isNeedChange)
				{
					var d:Number = 0;
					var p:Vec2;
					while (d < 100)
					{
						p = new Vec2();
						p.x = AntMath.randomRangeInt(leftB, rightB);
						p.y = AntMath.randomRangeInt(topB, botB);
						d = AntMath.distance(p.x, p.y, targetPoint.x, targetPoint.y);
					}
					targetPoint = p;
				}
			}
			
			if (targetPoint == null)
			{
				var r:Number = AntMath.getRotAngle(position.x, position.y, position.x, position.y + 10, degAngle, rotationSpeed);
				body.rotation += r*AntMath.TO_RADIANS;
				var vec:Vec2 = AntMath.getMoveVector(speed, degAngle);
				body.position.x += vec.x * universe.xt;
				body.position.y += vec.y * universe.xt;
			}
			
			if (isShoot && shootTimer.isTick && universe.isInBounds(position.x, position.y, 0, 0)) shoot();
		}
		
		protected function shoot():void
		{
			
		}
		
		public function getHealthSystem():HealthSystem
		{
			return health;
		}
		
		public function onDamage():void
		{
			if (white !== null && white.alpha == 0)
			{
				TweenLite.to(white, 0.1, { alpha:0.85, onComplete:function()
				{
					if(isExist) TweenLite.to(white, 0.06, { alpha:0 } );
				}});
			}
		}
		
		public function liquidate():void
		{
			Game.getInstance().soundManager.playSound(liquidateSound);
			HeroParametrs.enemies ++;
			destroy();
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (!universe.isInBounds(position.x, position.y, 300, 300))
			{
				super.checkOnDestroy(true);
			}
			
			super.checkOnDestroy(false);
		}
		
		override public function destroy():void
		{			
			path = null;
			pathDef = null;
			health.destroy();
			health = null;
			
			if (shootTimer !== null) shootTimer.destroy();
			if (waitOnPointTimer !== null) waitOnPointTimer.destroy();
			
			shootTimer = null;
			waitOnPointTimer = null;
			
			count --;
			last = universe.position;
			
			if (white !== null) this.removeChild(white);
			white = null;
			
			
			super.destroy();
		}			
	}
}