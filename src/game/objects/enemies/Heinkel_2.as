package src.game.objects.enemies 
{
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletE100;
	import src.game.objects.bullets.BulletH100;
	import src.game.objects.effects.ExplosionE100;
	import src.game.objects.others.Coin;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Heinkel_2 extends BasicEnemy
	{
		private var timet:FrameTimer;
		private var count:int;
		
		public static function create(posX:Number, posY:Number, rot:Number, pathName:String, speed:Number):Heinkel_2
		{
			var object:Heinkel_2 = new Heinkel_2();
			object.isUsePath = true;
			object.pathName = pathName;
			object.speed = speed;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function Heinkel_2() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{			
			actor = new AntActor("Heinkel2_mc");
			
			id.addTags([Kind.T_HEINKEL_2]);
			
			rotationSpeed = 2;
			maxHealth = 16;
			reload = 170;
			isShoot = true;
			
			timet = new FrameTimer(20);
			timet.stop();
			count = 2;
			
			white = new Heinkel2White_mc();
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(30, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (targetPoint !== null)
			{
				var r:Number = AntMath.getRotAngle(position.x, position.y, targetPoint.x, targetPoint.y, degAngle, rotationSpeed);
				body.rotation += r*AntMath.TO_RADIANS;
				var vec:Vec2 = AntMath.getMoveVector(speed, degAngle);
				body.position.x += vec.x * universe.xt;
				body.position.y += vec.y * universe.xt;
			}
		}
		
		override protected function shoot():void
		{
			shootTimer.stop();
			timet.start();
			
			if (count > 0)
			{
				if (timet.isTick && universe.hero.isExist)
				{
					var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
					
					BulletE100.create(position.x, position.y, a + AntMath.randomRangeInt(-5, 5), 4);
					count --;
				}
			}
			else
			{
				count = 2;
				shootTimer.start();
			}
		}
		
		override public function liquidate():void
		{
			ExplosionE100.create(position.x, position.y);
			if (AntMath.randomRangeInt(1, 100) < 55) Coin.create(position.x, position.y);
			
			
			super.liquidate();
		}
		
		override public function destroy():void
		{			
			timet.destroy();
			timet = null;
			
			
			super.destroy();
		}
		
	}
}