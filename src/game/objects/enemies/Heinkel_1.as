package src.game.objects.enemies 
{
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.core.Game;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletE100;
	import src.game.objects.bullets.BulletH100;
	import src.game.objects.effects.ExplosionE100;
	import src.game.objects.others.Coin;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Heinkel_1 extends BasicEnemy
	{						
		public static function create(posX:Number, posY:Number, rot:Number, pathName:String, speed:Number, isShoot:Boolean):Heinkel_1
		{
			var object:Heinkel_1 = new Heinkel_1();
			object.isUsePath = true;
			object.pathName = pathName;
			object.speed = speed;
			object.isShoot = isShoot;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function Heinkel_1() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{			
			actor = new AntActor("Heinkel1_mc");
			
			id.addTags([Kind.T_HEINKEL_1]);
			
			rotationSpeed = 2;
			maxHealth = 8;
			reload = 160;
			
			white = new Heinkel1White_mc();
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(30, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (targetPoint !== null)
			{
				var r:Number = AntMath.getRotAngle(position.x, position.y, targetPoint.x, targetPoint.y, degAngle, rotationSpeed);
				body.rotation += r*AntMath.TO_RADIANS;
				var vec:Vec2 = AntMath.getMoveVector(speed, degAngle);
				body.position.x += vec.x * universe.xt;
				body.position.y += vec.y * universe.xt;
			}
		}
		
		override protected function shoot():void
		{
			if (universe.hero.isExist)
			{
				var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
				
				BulletE100.create(position.x, position.y, a + AntMath.randomRangeInt(-10, 10), 3);
			}
		}
		
		override public function liquidate():void
		{			
			ExplosionE100.create(position.x, position.y);
			if (AntMath.randomRangeInt(1, 100) < 40) Coin.create(position.x, position.y);
			
			
			super.liquidate();
		}
		
		override public function destroy():void
		{			
			super.destroy();
		}			
	}
}