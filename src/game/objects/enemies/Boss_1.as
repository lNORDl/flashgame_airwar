package src.game.objects.enemies 
{
	import com.greensock.TweenLite;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletE100;
	import src.game.objects.bullets.BulletE200;
	import src.game.objects.bullets.BulletE300;
	import src.game.objects.effects.ExplosionE100;
	import src.game.objects.effects.ExplosionE200;
	import src.game.objects.effects.ExplosionE300;
	import src.game.objects.effects.ExplosionE50;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Boss_1 extends BasicObject implements IDestoyable
	{
		public static var isEx:Boolean = false;
		
		private var health:HealthSystem;
		
		private var left:AntActor;
		private var right:AntActor;
		
		private var phase:int;
		
		private var speed:int;
		
		private var movePoint:Vec2;
		private var moveTimer:FrameTimer;
		
		private var weapon1Timer:FrameTimer;
		private var weapon2Timer:FrameTimer;
		private var delay1Timer:FrameTimer;
		private var weapon1Count:int;
		private var help1Timer:FrameTimer;
		
		private var turret:AntActor;
		
		private var bossWhite:AntActor;
		
		public static function create():void
		{
			var boss:Boss_1 = new Boss_1();
			boss.init(250, -100, 0);
		}
		
		public function Boss_1() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{			
			container = universe.centergroundContainer;
			actor = new AntActor("BossCenter_mc");
			
			id.addTags([Kind.T_BOSS_1, Kind.GROUP_B]);
			
			health = new HealthSystem(this, 1500);
			
			left = new AntActor("BossLeft_mc");
			left.x = -10;
			left.y = 40;
			this.addChild(left);
			right = new AntActor("BossRight_mc");
			right.x = 10;
			right.y = 40;
			this.addChild(right);
			
			Boss_1.isEx = true;
			
			phase = 1;
			
			
			super.init(posX, posY, rot);
			
			
			turret = new AntActor("BossTurret_mc");
			turret.x = -1.3;
			turret.y = 3.4;
			turret.rotation = 90;
			turret.smoothing = true;
			this.addChild(turret);
			
			bossWhite = new AntActor("BossWhite_mc");
			this.addChild(bossWhite);
			bossWhite.alpha = 0;
			
			
			moveTimer = new FrameTimer(400, 400);			
			speed = 1.2;
			
			weapon1Timer = new FrameTimer(100, 0);
			weapon2Timer = new FrameTimer(500, 0);
			delay1Timer = new FrameTimer(30, 30);
			help1Timer = new FrameTimer(0, 0);
			weapon1Count = 5;
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Polygon = new Polygon([new Vec2(-10, -100), new Vec2(10, -100), new Vec2(10, 90), new Vec2(-10, 90)], material);
			var shape1:Polygon = new Polygon([new Vec2(0, 0), new Vec2(150, 0), new Vec2(150, 40), new Vec2(0, 60)], material);
			var shape2:Polygon = new Polygon([new Vec2(0, 0), new Vec2(-150, 0), new Vec2(-150, 40), new Vec2(0, 60)], material);
			shape.sensorEnabled = true;
			shape1.sensorEnabled = true;
			shape2.sensorEnabled = true;
			body.shapes.add(shape);
			body.shapes.add(shape1);
			body.shapes.add(shape2);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (isNextPhase()) 
			{
				//trace(health.curHealth);
				nextPhase();
			}
			
			if (moveTimer.isTick || movePoint == null)
			{
				movePoint = new Vec2(AntMath.randomRangeInt(165, 345), AntMath.randomRangeInt(106, 156));
			}
			
			if (AntMath.distance(position.x, position.y, movePoint.x, movePoint.y) > speed)
			{
				var vec:Vec2 = AntMath.getMoveVector(speed, AntMath.angle(position.x, position.y, movePoint.x, movePoint.y) * AntMath.TO_DEGREES);
				body.position.x += vec.x;
				body.position.y += vec.y;
			}
			
			if (phase == 1) phase1();
			else if (phase == 2) phase2();
			else if (phase == 3) phase3();
			else if (phase == 4) phase4();
			
			this.body.position.x = Math.round(this.body.position.x);
			this.body.position.y = Math.round(this.body.position.y);
		}
		
		private function phase1():void
		{
			if (weapon1Timer.isTick)
			{
				weapon1Timer.stop();
				delay1Timer.start();
				if (delay1Timer.isTick)
				{
					if (weapon1Count == 0)
					{
						weapon1Count = 5;
						delay1Timer.stop();
						weapon1Timer.start();
					}
					else
					{
						weapon1Count --;
						shoot(1);
					}
				}
			}	
			
			if (weapon2Timer.isTick) shoot(2);
		}
		
		private function phase2():void
		{
			if (weapon1Timer.isTick)
			{
				weapon1Timer.stop();
				delay1Timer.start();
				if (delay1Timer.isTick)
				{
					if (weapon1Count == 0)
					{
						weapon1Count = 4;
						delay1Timer.stop();
						weapon1Timer.start();
					}
					else
					{
						weapon1Count --;
						shoot(3);
					}
				}
			}	
			
			if (weapon2Timer.isTick) shoot(4);
			
			if (help1Timer.isTick)
			{
				Heinkel_4.create(70, -100, 90, "", 5);
				Heinkel_4.create(430, -100, 90, "", 5);
			}
		}
		
		private function phase3():void
		{
			var r:Number = AntMath.getRotAngle(position.x - 1.3, position.y + 3.4, universe.hero.x, universe.hero.y, turret.rotation, 1);
			turret.rotation += r;
			
			if (weapon1Timer.isTick)
			{
				weapon1Timer.stop();
				delay1Timer.start();
				if (delay1Timer.isTick)
				{
					if (weapon1Count == 0)
					{
						weapon1Count = 10;
						delay1Timer.stop();
						weapon1Timer.start();
					}
					else
					{
						weapon1Count --;
						var p:Vec2 = AntMath.getMoveVector(39, turret.rotation).add(position);
						BulletE200.create(p.x, p.y, turret.rotation + AntMath.randomRangeInt(-5, 5), 7, true);	
					}
				}
			}
			
			if (weapon2Timer.isTick) 
			{
				var n:int = 30;
				BulletE300.create(position.x + 60, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 60, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 90, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 90, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
			}
			
			if (help1Timer.isTick)
			{
				Heinkel_4.create(70, -100, 90, "", 5);
				Heinkel_4.create(430, -100, 90, "", 5);
			}
		}
		
		private function phase4():void
		{
			//var r:Number = AntMath.getRotAngle(position.x - 1.3, position.y + 3.4, universe.hero.x, universe.hero.y, turret.rotation, 1);
			turret.rotation += 4;
			
			if (weapon1Timer.isTick)
			{
				weapon1Timer.stop();
				delay1Timer.start();
				if (delay1Timer.isTick)
				{
					if (weapon1Count == 0)
					{
						weapon1Count = 10;
						delay1Timer.stop();
						weapon1Timer.start();
					}
					else
					{
						weapon1Count --;
						var p:Vec2 = AntMath.getMoveVector(39, turret.rotation).add(position);
						BulletE200.create(p.x, p.y, turret.rotation, 10, true);	
					}
				}
			}
			
			if (weapon2Timer.isTick) 
			{
				var n:int = 30;
				BulletE300.create(position.x + 60, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 60, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 90, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
				BulletE300.create(position.x - 90, position.y + 25, AntMath.randomRangeInt(90-n, 90+n), 2, 0);
			}
			
			if (help1Timer.isTick)
			{
				Heinkel_4.create(70, -100, 45, "", 5);
				Heinkel_4.create(430, -100, 135, "", 5);
				Heinkel_4.create(-100, 450, 0, "", 5);
				Heinkel_4.create(600, 450, 180, "", 5);
			}
		}
		
		private function shoot(t:int, p:Array = null):void
		{
			if (t == 1)
			{
				var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
				BulletE100.create(position.x + 40, position.y + 25, a + AntMath.randomRangeInt( -3, 3), 5);						
				BulletE100.create(position.x, position.y + 5, a + AntMath.randomRangeInt(-3, 3), 5);
				BulletE100.create(position.x - 40, position.y + 25, a + AntMath.randomRangeInt(-3, 3), 5);
			}
			else if (t == 2)
			{
				BulletE300.create(position.x + 40, position.y + 25, 90, 4, 2);
				BulletE300.create(position.x - 40, position.y + 25, 90, 4, 2);
			}
			
			if (t == 3)
			{
				var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
				BulletE200.create(position.x + 40, position.y + 25, a + AntMath.randomRangeInt( -3, 3), 7);						
				BulletE200.create(position.x + 80, position.y + 5, a + AntMath.randomRangeInt(-3, 3), 7);
				BulletE200.create(position.x - 80, position.y + 5, a + AntMath.randomRangeInt(-3, 3), 7);
				BulletE200.create(position.x - 40, position.y + 25, a + AntMath.randomRangeInt(-3, 3), 7);
			}
			else if (t == 4)
			{
				BulletE300.create(position.x + 60, position.y + 25, 90, 2, 1.2);
				BulletE300.create(position.x, position.y + 25, 90, 2, 1.2);
				BulletE300.create(position.x - 60, position.y + 25, 90, 2, 1.2);
				BulletE300.create(position.x - 90, position.y + 25, 90, 2, 1.2);
				BulletE300.create(position.x - 90, position.y + 25, 90, 2, 1.2);
			}
		}
		
		private function isNextPhase():Boolean
		{
			return ((phase == 1 && health.curHealth < 1000) ||
					(phase == 2 && health.curHealth < 500) ||
					(phase == 3 && health.curHealth < 250));
		}
		
		private function nextPhase():void
		{
			if (phase == 1)
			{
				left.gotoAndStop(2);
				ExplosionE50.create(position.x - 110, position.y + 40);
				ExplosionE50.create(position.x - 56, position.y + 20);
				ExplosionE50.create(position.x - 32, position.y + 46);
				ExplosionE200.create(position.x - 74, position.y + 32);
				
				weapon1Timer = new FrameTimer(75, 0);
				weapon2Timer = new FrameTimer(300, 0);
				delay1Timer = new FrameTimer(10, 10);
				weapon1Count = 4;
				
				help1Timer = new FrameTimer(900, 0);
			}
			else if (phase == 2)
			{
				right.gotoAndStop(2);
				ExplosionE50.create(position.x + 110, position.y + 40);
				ExplosionE50.create(position.x + 56, position.y + 20);
				ExplosionE50.create(position.x + 32, position.y + 46);
				ExplosionE200.create(position.x + 74, position.y + 32);
				
				weapon1Timer = new FrameTimer(100, 0);
				weapon2Timer = new FrameTimer(300, 0);
				delay1Timer = new FrameTimer(5, 5);
				weapon1Count = 10;
				
				help1Timer = new FrameTimer(500, 0);
			}
			else if (phase == 3)
			{
				actor.gotoAndStop(2);
				
				weapon1Timer = new FrameTimer(20, 0);
				delay1Timer = new FrameTimer(2, 2);
			}
			else if (phase == 4)
			{
				//liquidate();
			}
			
			phase ++;
		}
		
		public function getHealthSystem():HealthSystem
		{
			return health;
		}
		
		public function onDamage():void
		{
			if (bossWhite.alpha == 0)
			{
				TweenLite.to(bossWhite, 0.1, { alpha:0.85, onComplete:function()
				{
					if(isExist) TweenLite.to(bossWhite, 0.06, { alpha:0 } );
				}});
			}
		}
		
		public function liquidate():void
		{	
			ExplosionE300.create(position.x + 22, position.y - 14);
			ExplosionE200.create(position.x - 9, position.y - 29);
			ExplosionE200.create(position.x + 59, position.y + 8);
			ExplosionE100.create(position.x + 68, position.y + 61);
			ExplosionE100.create(position.x - 26, position.y - 37);
			ExplosionE100.create(position.x - 80, position.y + 30);
			
			Boss_1.isEx = false;
			
			destroy();
		}
		
		override public function destroy():void
		{			
			health.destroy();
			health = null;
			
			left.free();
			left = null;
			right.free();
			right = null;
			
			moveTimer.destroy();
			weapon1Timer.destroy();
			weapon2Timer.destroy();
			delay1Timer.destroy();
			help1Timer.destroy();
			
			moveTimer = null;
			weapon1Timer = null;
			weapon2Timer = null;
			delay1Timer = null;
			help1Timer = null;
			
			bossWhite.free();
			bossWhite = null;
			
			BasicEnemy.last = universe.position;
			
			
			super.destroy();
		}	
		
	}

}