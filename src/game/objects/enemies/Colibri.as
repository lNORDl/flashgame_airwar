package src.game.objects.enemies 
{
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletE100;
	import src.game.objects.bullets.BulletE200;
	import src.game.objects.bullets.BulletH100;
	import src.game.objects.effects.ExplosionE100;
	import src.game.objects.effects.ExplosionE200;
	import src.game.objects.effects.ExplosionE300;
	import src.game.objects.others.Coin;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Colibri extends BasicEnemy
	{		
		private var timet:FrameTimer;
		private var count:int;
		private var planes:AntActor;
		
		public static function create(posX:Number, posY:Number, leftB:int, rightB:int, topB:int, botB:int):Colibri
		{
			var object:Colibri = new Colibri();
			object.isUseBounds = true;
			object.leftB = leftB;
			object.rightB = rightB;
			object.topB = topB;
			object.botB = botB;
			object.time = 100;
			object.init(posX, posY, 90);
			
			return object;
		}
		
		public function Colibri() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{			
			actor = new AntActor("Colibri_mc");
			
			id.addTags([Kind.T_COLIBRI]);
			
			maxHealth = 65;
			reload = 120;
			isShoot = true;
			speed = 2;
			
			timet = new FrameTimer(8);
			timet.stop();
			count = 12;
			
			white = new ColibriWhite_mc();
			
			
			super.init(posX, posY, rot);
			
			
			planes = new AntActor("ColibriPlanes_mc");
			this.addChild(planes);
			planes.rotation = 90;
			planes.play();
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(50, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (targetPoint !== null)
			{
				var d:Number = AntMath.distance(position.x, position.y, targetPoint.x, targetPoint.y);
				if (d > 5)
				{
					var vec:Vec2 = AntMath.getMoveVector(speed, AntMath.angle(position.x, position.y, targetPoint.x, targetPoint.y)*AntMath.TO_DEGREES);
					body.position.x += vec.x * universe.xt;
					body.position.y += vec.y * universe.xt;
				}
			}
		}
		
		override protected function shoot():void
		{
			shootTimer.stop();
			timet.start();
			
			if (count > 0)
			{
				if (timet.isTick)
				{
					if (universe.hero.isExist)
					{
						var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
						BulletE200.create(position.x, position.y + 10, a + AntMath.randomRangeInt( -15, 15), 5);						
					}
					
					count --;
				}
			}
			else
			{
				count = 12;
				shootTimer.start();
			}
		}
		
		override public function liquidate():void
		{
			ExplosionE300.create(position.x, position.y);
			
			var n:int = 5;
			for (var i:int = 0; i < n; i++)
			{
				var p:Vec2 = AntMath.getRandomPointInRadius(30);
				Coin.create(position.x + p.x, position.y + p.y);
			}
			
			
			super.liquidate();
		}
		
		override public function destroy():void
		{			
			timet.destroy();
			timet = null;
			
			planes.free();
			planes = null;
			
			
			super.destroy();
		}			
	}
}