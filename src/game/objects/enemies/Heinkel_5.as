package src.game.objects.enemies 
{
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.health.HealthSystem;
	import src.framework.health.IDestoyable;
	import src.game.objects.BasicObject;
	import src.game.objects.bullets.BulletE100;
	import src.game.objects.bullets.BulletE200;
	import src.game.objects.bullets.BulletE300;
	import src.game.objects.bullets.BulletH100;
	import src.game.objects.effects.ExplosionE100;
	import src.game.objects.others.Coin;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Heinkel_5 extends BasicEnemy
	{
		private var timet:FrameTimer;
		private var st:FrameTimer;
		private var count:int;
		
		public static function create(posX:Number, posY:Number, rot:Number, pathName:String, speed:Number):Heinkel_5
		{
			var object:Heinkel_5 = new Heinkel_5();
			object.isUsePath = true;
			object.pathName = pathName;
			object.speed = speed;
			object.init(posX, posY, rot);
			
			return object;
		}
		
		public function Heinkel_5() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{			
			actor = new AntActor("Heinkel5_mc");
			
			id.addTags([Kind.T_HEINKEL_5]);
			
			rotationSpeed = 2;
			maxHealth = 120;
			reload = 150;
			isShoot = true;
			
			timet = new FrameTimer(10);
			timet.stop();
			count = 3;
			
			white = new Heinkel5White_mc();
			
			st = new FrameTimer(300, 100);
			
			liquidateSound = Kind.S_EXPLOSION_E200;
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(45, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			if (targetPoint !== null)
			{
				//var r:Number = AntMath.getRotAngle(position.x, position.y, targetPoint.x, targetPoint.y, degAngle, rotationSpeed);
				//body.rotation += r*AntMath.TO_RADIANS;
				var vec:Vec2 = AntMath.getMoveVector(speed, AntMath.angle(position.x, position.y, targetPoint.x, targetPoint.y)*AntMath.TO_DEGREES);
				body.position.x += vec.x * universe.xt;
				body.position.y += vec.y * universe.xt;
			}
			
			if (st.isTick && universe.hero.isExist)
			{
				var a:Number = AntMath.angle(position.x, position.y, universe.hero.position.x, universe.hero.position.y) * AntMath.TO_DEGREES;
				//BulletE200.create(position.x, position.y - 20, a, 5);
				//BulletE200.create(position.x, position.y + 5, a, 5);
				BulletE300.create(position.x, position.y + 5, a, 2.5, 1);
			}
		}
		
		override protected function shoot():void
		{
			shootTimer.stop();
			timet.start();
			
			if (count > 0)
			{
				if (timet.isTick)
				{
					var p:Vec2;
					p = AntMath.getMoveVector(20, degAngle + 90);
					BulletE100.create(position.x + p.x, position.y + p.y, degAngle, 7);
					p = AntMath.getMoveVector(20, degAngle - 90);
					BulletE100.create(position.x + p.x, position.y + p.y, degAngle, 7);
					count --;
				}
			}
			else
			{
				count = 3;
				shootTimer.start();
			}
		}
		
		override public function liquidate():void
		{
			ExplosionE100.create(position.x, position.y);
			var n:int = 3;
			for (var i:int = 0; i < n; i++)
			{
				var p:Vec2 = AntMath.getRandomPointInRadius(30);
				Coin.create(position.x + p.x, position.y + p.y);
			}
			
			
			super.liquidate();
		}
		
		override public function destroy():void
		{			
			timet.destroy();
			timet = null;
			
			st.destroy();
			st = null;
			
			
			super.destroy();
		}
	}
}