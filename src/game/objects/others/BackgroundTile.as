package src.game.objects.others 
{
	import src.framework.animation.AntActor;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.id.IdFilter;
	import src.game.core.Universe;
	import src.game.levels.BasicLevel;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class BackgroundTile extends BasicObject
	{
		public static var sh:Number;
		
		private static var count:int = 0;
		private static var last:int = -1;
		
		public var tile:String;
		private var isC:Boolean;
		private var h:int;
		
		public static function create(posY:Number, tile:String):BackgroundTile
		{
			var object:BackgroundTile = new BackgroundTile();
			object.tile = tile;
			object.init(0, posY, 0);
			
			return object;
		}
		
		public function BackgroundTile() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.backgroundContainer;
			
			actor = new AntActor(tile);
			//actor.smoothing = true;
			
			
			super.init(posX, posY, rot);
			
			
			universe.backgroundContainer.removeChild(this);
			universe.backgroundContainer.addChildAt(this, 0);
			
			count ++;
			isC = false;
			
			switch(tile)
			{
				case "Bg_1": h = 360; break;
				case "Bg_2": h = 180; break;
				case "Bg_3": h = 180; break;
				case "Bg_4": h = 180; break;
				case "Bg_5": h = 180; break;
				case "Bg_6": h = 180; break;
				case "Bg_7": h = 180; break;
				case "Bg_8": h = 180; break;
				case "Bg_9": h = 180; break;
				case "Bg_11": h = 185; break;
				case "Bg_12": h = 185; break;
				case "Bg_13": h = 185; break;
				case "Bg_14": h = 185; break;
				case "Bg_15": h = 185; break;
				case "Bg_16": h = 185; break;
				case "Bg_17": h = 185; break;
				case "Bg_18": h = 185; break;
				case "Bg_19": h = 185; break;
			}
		}
		
		override public function update():void
		{
			super.update();
			
			
			this.y += sh;
			
			if (count < 7 && this.y - h + 10 > 0 && !isC)
			{
				var x:String = "";
				x = (BasicLevel.levelType == BasicLevel.WINTER)?"1":"";
				var i:int = last;
				while (i == last) i = AntMath.randomRangeInt(1, 9);
				BackgroundTile.create(this.y - h + 1.5, "Bg_"+x+i.toString());
				isC = true;
				last = i;
			}
			
			this.y = Math.round(this.y);
		}
		
		public static function createTile():void
		{
			
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (this.y > 600 + h + 50) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}	
		
		override public function destroy():void
		{
			count --;
			
			
			super.destroy();
		}
	}
}