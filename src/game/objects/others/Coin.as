package src.game.objects.others 
{
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Shape;
	import src.framework.animation.AntActor;
	import src.framework.health.IDestoyable;
	import src.game.core.Game;
	import src.game.objects.BasicObject;
	import src.game.objects.enemies.Heinkel_1;
	import src.game.objects.hero.Hero;
	import src.game.objects.hero.HeroParametrs;
	import src.utils.AntMath;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Coin extends BasicObject implements IBonus
	{
		public static function create(posX:Number, posY:Number):Coin
		{
			var object:Coin = new Coin();
			object.init(posX, posY, 0);
			
			return object;
		}
		
		public function Coin() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.bulletsContainer;
			
			actor = new AntActor("Coin_mc");
			actor.play();
			
			id.addTags([Kind.GROUP_N, Kind.T_BONUS]);
			
			
			super.init(posX, posY, rot);
		}
		
		override protected function initBody(data:* = null):void
		{
			body = new Body(BodyType.KINEMATIC, position);
			body.rotation = radAngle;
			var material:Material = Material.wood();
			var shape:Shape = new Circle(10, new Vec2(0, 0), material);
			shape.sensorEnabled = true;
			body.shapes.add(shape);
			
			super.initBody(this);
		}
		
		override public function update():void
		{
			super.update();
			
			
			var isMagnit:Boolean = false;
			var h:Hero = universe.hero;
			var s:Number = 3;
			if (h.isExist)
			{
				var d:Number = AntMath.distance(position.x, position.y, h.position.x, h.position.y);
				if (d < 100)
				{
					isMagnit = true;
					s = s * (100 / d);
					var v:Vec2 = AntMath.getMoveVector(s, AntMath.angle(position.x, position.y, h.position.x, h.position.y) * AntMath.TO_DEGREES);
					body.position.x += v.x * universe.xt;
					body.position.y += v.y * universe.xt;
				}
			}
			
			if (!isMagnit)
			{
				body.position.y += universe.speed * universe.xt + 0.5;
			}
		}
		
		public function activate(object:*):void
		{
			Game.getInstance().soundManager.playSound(Kind.S_COIN_PICKUP);
			HeroParametrs.changeCoins(1);
			destroy();
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (!universe.isInBounds(position.x, position.y, 200, 200)) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}
		
		override public function destroy():void
		{						
			super.destroy();
		}			
	}
}