package src.game.objects.others 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Fog extends BasicObject
	{
		public static function create(posX:Number, posY:Number):Fog
		{
			var object:Fog = new Fog();
			object.init(posX, posY, 0);
			
			return object;
		}
		
		public function Fog() 
		{
			
		}		
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("Fog_mc");
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update():void
		{
			super.update();
			
			
			this.y += universe.speed*3;
		}
		
		override protected function checkOnDestroy(c:Boolean = false):void
		{
			if (this.y > 850) super.checkOnDestroy(true);
			
			super.checkOnDestroy(false);
		}		
	}
}