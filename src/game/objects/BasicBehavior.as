package src.game.objects 
{
	import nape.phys.Body;
	import src.game.core.Universe;
	/**
	 * ...
	 * @author ...
	 */
	public class BasicBehavior
	{
		protected var universe:Universe = Universe.getInstance();
		protected var object:BasicObject;
		protected var body:Body;
		
		public function BasicBehavior() 
		{
			
		}
		
		public function init(object:BasicObject):void
		{
			this.object = object;		
			body = object.body;
			object.behaviors.add(this);
		}
		
		public function update():void
		{
			
		}
		
		public function destroy():void
		{
			object.behaviors.remove(this);
			
			object = null;			
			universe = null;
		}		
	}
}