package src.game.objects.effects 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ExplosionE300 extends BasicObject
	{
		
		public static function create(posX:Number, posY:Number):ExplosionE300
		{
			var object:ExplosionE300 = new ExplosionE300();
			object.init(posX, posY, AntMath.randomRangeInt(0, 360));
			
			return object;
		}
		
		public function ExplosionE300() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("ExplosionE300_mc");
			actor.play();
			actor.onCompleteCallback = function (){ destroy(); };
			
			
			super.init(posX, posY, rot);
		}
	}
}