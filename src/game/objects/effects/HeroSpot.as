package src.game.objects.effects 
{
	import com.greensock.TweenLite;
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class HeroSpot extends BasicObject
	{
		private static var instance:HeroSpot;
		
		private var s1:AntActor;
		private var s2:AntActor;
		private var s3:AntActor;
		
		public static function create():void
		{
			if (instance == null)
			{
				var object:HeroSpot = new HeroSpot();
				object.init(0, 0, 0);
			}
			
			instance.start();
		}
		
		public function HeroSpot() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("Spot_mc");
			
			
			super.init(posX, posY, rot);
			
			
			this.alpha = 0;
			instance = this;
			
			s1 = new AntActor("Spot_mc");
			this.addChild(s1);
			s1.x = 500;
			s1.y = 0;
			
			s2 = new AntActor("Spot_mc");
			this.addChild(s2);
			s2.x = 500;
			s2.y = 600;	
			
			s3 = new AntActor("Spot_mc");
			this.addChild(s3);
			s3.x = 0;
			s3.y = 600;
		}	
		
		public function start():void
		{
			TweenLite.to(this, 0.3, { alpha:0.6, onComplete:onShow } );
		}
		
		private function onShow():void
		{
			TweenLite.to(this, 0.6, { alpha:0, delay:0.4} );
		}
		
		override public function destroy():void
		{
			instance = null;
			
			
			super.destroy();
		}
	}
}