package src.game.objects.effects 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ExplosionE100 extends BasicObject
	{
		public static function create(posX:Number, posY:Number):ExplosionE100
		{
			var object:ExplosionE100 = new ExplosionE100();
			object.init(posX, posY, AntMath.randomRangeInt(0, 360));
			
			return object;
		}
		
		public function ExplosionE100() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("ExplosionE100_mc");
			actor.play();
			actor.onCompleteCallback = function (){ destroy(); };
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update():void
		{
			super.update();
		}
		
		override public function destroy():void
		{			
			super.destroy();
		}	
		
	}

}