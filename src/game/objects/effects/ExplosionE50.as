package src.game.objects.effects 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ExplosionE50 extends BasicObject
	{
		
		public static function create(posX:Number, posY:Number):ExplosionE50
		{
			var object:ExplosionE50 = new ExplosionE50();
			object.init(posX, posY, AntMath.randomRangeInt(0, 360));
			
			return object;
		}
		
		public function ExplosionE50() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("ExplosionE50_mc");
			actor.width *= 0.7;
			actor.height *= 0.7;
			actor.play();
			actor.onCompleteCallback = function (){ destroy(); };
			
			
			super.init(posX, posY, rot);
		}		
	}
}