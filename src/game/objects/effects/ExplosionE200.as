package src.game.objects.effects 
{
	import src.framework.animation.AntActor;
	import src.game.objects.BasicObject;
	import src.utils.AntMath;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class ExplosionE200 extends BasicObject
	{
		public static function create(posX:Number, posY:Number):ExplosionE200
		{
			var object:ExplosionE200 = new ExplosionE200();
			object.init(posX, posY, AntMath.randomRangeInt(0, 360));
			
			return object;
		}
		
		public function ExplosionE200() 
		{
			
		}
		
		override public function init(posX:Number, posY:Number, rot:Number):void
		{
			container = universe.overgroundContainer;
			
			actor = new AntActor("ExplosionE200_mc");
			actor.play();
			actor.onCompleteCallback = function (){ destroy(); };
			
			
			super.init(posX, posY, rot);
		}
		
		override public function update():void
		{
			super.update();
		}
		
		override public function destroy():void
		{			
			super.destroy();
		}	
	}
}