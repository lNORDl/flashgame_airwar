package src.game.core 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import src.game.core.managers.SoundManager;
	import src.game.core.managers.WindowsManager;
	import src.game.interfaces.windows.screens.ScreenGame;
	import src.game.interfaces.windows.screens.ScreenMainMenu;
	import src.utils.Kind;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Game extends Sprite
	{		
		public var windowsManager:WindowsManager;
		public var soundManager:SoundManager;
		public var rootLayer:Sprite;			
		
		private static var _instance:Game;
		private var universe:Universe;
		
		public function Game() 
		{
			_instance = this;
			
			rootLayer = new Sprite();
			this.addChild(rootLayer);
			
			// Universe
			universe = Universe.getInstance();
			rootLayer.addChild(universe);
			
			soundManager = new SoundManager();
			soundManager.init();
			
			windowsManager = new WindowsManager();
			windowsManager.init();
			
			this.addEventListener(Event.ENTER_FRAME, gameLoop);
		}
		
		private function gameLoop(e:Event):void 
		{
			universe.update();
			windowsManager.update();
		}		
		
		public static function getInstance():Game {return _instance;}
	}
}