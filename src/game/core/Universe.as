package src.game.core 
{
	import com.greensock.TweenLite;
	import flash.Boot;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import nape.callbacks.PreFlag;
	import nape.dynamics.Arbiter;
	import nape.dynamics.ArbiterList;
	import nape.dynamics.ArbiterType;
	import nape.dynamics.InteractionFilter;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.shape.Shape;
	import nape.space.Space;
	import nape.util.Debug;
	import nape.util.ShapeDebug;
	import src.framework.frameTimer.FrameTimer;
	import src.framework.id.IdFilter;
	import src.framework.particleSystem.Particle;
	import src.framework.particleSystem.ParticleSettings;
	import src.game.interfaces.windows.panels.PanelPause;
	import src.game.interfaces.windows.screens.ScreenGame;
	import src.game.levels.BasicLevel;
	import src.game.objects.BasicObject;
	import src.game.objects.enemies.Heinkel_1;
	import src.game.objects.hero.Hero;
	import src.game.objects.hero.HeroParametrs;
	import src.game.objects.others.BackgroundTile;
	import src.utils.AntMath;
	import src.utils.Kind;
	import src.utils.ObjectsStorage;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Universe extends MovieClip
	{
		// STATIC
		private static var instance:Universe;
		
		public static var FPS:int = 60;
		public static const GAME_WIDTH:int = 500;
		public static const GAME_HEIGHT:int = 600;
		
		// PUBLIC
		public var levelContainer:MovieClip;
		public var backgroundContainer:MovieClip;
		public var overgroundContainer:MovieClip;
		public var centergroundContainer:MovieClip;
		public var heroContainer:MovieClip;
		public var bulletsContainer:MovieClip;
		
		public var gStage:Stage;
		
		public var objects:ObjectsStorage;		
		private var frameTimers:Array = [];		
		private var contacts:Array;
		private var onRemove:Array;			
		public var space:Space;
		public var debug:Debug;		
		private var paths:Array;
		
		private var level:BasicLevel;
		
		public var dt:Number;
		public var xt:Number;
		private var _t:Number;
		
		public var hero:Hero;
		public var position:Number;
		public var speed:Number;
		public var isPLay:Boolean;
		private var isPaused:Boolean;
		
		private var functions:Array;
		
		public function Universe() 
		{
			_t = getTimer();
			
			initContainers();
			
			initPaths();
			
			this.addEventListener(Event.ADDED_TO_STAGE, init);			
			instance = this;
			
			new Boot();
			space = new Space(new Vec2(0, 0));			
			debug = new ShapeDebug(450, 600, 0xFFFFFF);
			overgroundContainer.addChild(debug.display);
			
			contacts = [];
			onRemove = [];
			
			functions = [];
			
			objects = new ObjectsStorage();	
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			gStage = stage;
			
			HeroParametrs.init();
			
			speed = 1.5;
			position = 600;
			
			level = new BasicLevel();
			BasicLevel.addLevel("Level_1", [new Level1_mc()]);
			BasicLevel.addLevel("Level_2", [new Level2_mc()]);
			BasicLevel.addLevel("Level_3", [new Level3_mc()]);
			BasicLevel.addLevel("Level_4", [new Level4_mc()]);
			BasicLevel.addLevel("Level_5", [new Level5_mc()]);
			BasicLevel.addLevel("Level_6", [new Level6_mc()]);
			BasicLevel.addLevel("Level_7", [new Level7_mc()]);
			BasicLevel.addLevel("Level_8", [new Level8_mc()]);
			BasicLevel.addLevel("Level_9", [new Level9_mc()]);
			BasicLevel.addLevel("Level_10", [new Level10_mc()]);
			
			levelContainer.alpha = 0;
		}
		
		private function initContainers():void
		{
			levelContainer = new MovieClip();
			addChild(levelContainer);
			
			backgroundContainer = new MovieClip();
			levelContainer.addChild(backgroundContainer);
			
			centergroundContainer = new MovieClip();			
			levelContainer.addChild(centergroundContainer);
			
			overgroundContainer = new MovieClip();			
			levelContainer.addChild(overgroundContainer);
			
			bulletsContainer = new MovieClip();
			centergroundContainer.addChild(bulletsContainer);
			
			heroContainer = new MovieClip();
			centergroundContainer.addChild(heroContainer);
		}
		
		public function startLevel(l:int):void
		{
			ScreenGame.lvl = l;
			Game.getInstance().windowsManager.switchScreen(Kind.SCREEN_GAME);
			HeroParametrs.curLvl = l;
		}
		
		public function beginLevel():void
		{			
			level.init("Level_" + ScreenGame.lvl.toString());
			isPLay = true;			
			trace("Level " + ScreenGame.lvl + " Start!");
		}
		
		public function endLevel():void
		{
			isPLay = false;
			objects.destroyAll();
			trace("Level destoyed, objects count: " + objects.objects.length);
			position = 600;
		}
		
		public function levelFailed():void
		{
			isPLay = false;
			Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_LEVEL_FAILED).show();
			HeroParametrs.saveData();
		}
		
		public function levelComplete():void
		{
			isPLay = false;
			if (HeroParametrs.curLvl < HeroParametrs.LVL_COUNT) 
			{
				HeroParametrs.curLvl ++;
				if (HeroParametrs.curLvl > HeroParametrs.maxLvl) HeroParametrs.maxLvl = HeroParametrs.curLvl;
			}
			Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_LEVEL_COMPLETE).show();
			HeroParametrs.saveData();
		}
		
		public function switchPause():void
		{
			if (hero !== null && hero.isExist && isPLay && position > 610)
			{
				var panel:PanelPause = Game.getInstance().windowsManager.getWindowByName(Kind.PANEL_PAUSE) as PanelPause;
				var n:Boolean = !isPaused;
				if(n) panel.show();
				else panel.hide();
				if (panel.isShow) isPaused = true;
				else if (panel.isHide) isPaused = false;
			}
		}
		
		public function update():void
		{		
			var t:int = getTimer();
			dt = (t - _t);
			xt = dt / 16.66666666666667;
			//xt = (xt > 1.7)?1.7:xt;
			_t = t;
			
			if (isPLay && !isPaused)
			{			
				space.step(1 / 60.0);
				debug.clear();
				BackgroundTile.sh = speed * xt;
				//debug.draw(space);			
				
				updateTimers();
				
				if(level !== null) level.update();
				
				updateContacts();
				
				objects.update();
				
				position += speed;
			}
			
			var onR:Array = [];
			for (var i:int = 0; i < functions.length; i++)
			{
				if (functions[i]()) onR.push(functions[i]);
			}
			for (var i:int = 0; i < onR.length; i++)
			{
				functions.splice(functions.indexOf(onR[i]), 1);
			}
			
			//if (functions.length !== 0) trace("Alarm!");
		}
		//===============================================CONTACTS================================================//
		private function updateContacts():void
		{
			for (var i:int = 0; i < contacts.length; i++)
			{
				var body:Body = contacts[i][0];
				var tags:Array = contacts[i][1];
				var type:int = contacts[i][2];
				var f:Function = contacts[i][3];
				
				for (var j:int = 0; j < body.arbiters.length; j++)
				{
					var a:Arbiter = body.arbiters.at(j);
					var contactBody:Body = (a.body1 == body)?a.body2:a.body1;
					var contactObject:BasicObject = contactBody.userData.Object;
					
					if ((type == Kind.OR && contactObject.id.checkOR(tags)))
					{
						f(contactObject);
					}
				}
			}
			
			for (var i:int = 0; i < onRemove.length; i++)
			{
				var n:int = contacts.indexOf(onRemove[i]);
				contacts.splice(n, 1);
			}
			
			onRemove = [];
		}
		
		public function addCheckContact(def:Array):void
		{
			contacts.push(def);
		}
		
		public function removeCheckContact(def:Array):void
		{
			if (contacts.indexOf(def) !== -1) onRemove.push(def);
		}
		//===============================================+++++++++==============================================//
		//===============================================TIMER==================================================//
		private function updateTimers():void 
		{
			var l:int = frameTimers.length;
			for (var i:int = 0; i < l; i++)
			{
				frameTimers[i].update();
			}
		}
		
		public function addTimer(t:FrameTimer):void
		{
			if (frameTimers.indexOf(t) == -1) frameTimers.push(t);
		}
		
		public function removeTimer(t:FrameTimer):void
		{
			var i:int = frameTimers.indexOf(t);
			if (i !== -1) frameTimers.splice(i, 1);
		}
		//===============================================+++++++++==============================================//
		
		public function isInBounds(x:Number, y:Number, w:int = 100, h:int = 100):Boolean
		{
			return ((x > -w) && (x < GAME_WIDTH + w) && (y > -h) && (y < GAME_HEIGHT + h));
		}
		
		private function initPaths():void
		{
			paths = [];
			var container:MovieClip = new Paths_mc();
			for (var i:int = 0; i < container.numChildren; i++)
			{
				var mc:MovieClip = container.getChildAt(i) as MovieClip;
				var path:Array = createPath(mc);
				paths.push(path);
			}
		}
		
		private function createPath(mc:MovieClip):Array
		{
			var path:Array = [];
			
			var def:PathDef_mc = mc.getChildByName("def") as PathDef_mc;
			path[0] = [def.n, def.type];
			path[1] = [];
			for (var i:int = 0; i < mc.numChildren; i++)
			{
				var dot:MovieClip = mc.getChildAt(i) as PathDot_mc;
				if(dot !== null) path[1].push(new Vec2(dot.x, dot.y));
			}	
			
			return path;
		}
		
		public function getPath(name:String):Array
		{
			for (var i:int = 0; i < paths.length; i++)
			{
				if (paths[i][0][0] == name) return paths[i].concat();
			}
			
			return [];
		}
		
		public function addFunction(f:Function):void
		{
			functions.push(f);
		}
		
		public static function getInstance():Universe
		{
			return (instance !== null)?instance:new Universe();
		}
	}
}