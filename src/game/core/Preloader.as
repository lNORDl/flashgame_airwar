package src.game.core 
{
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.text.TextField;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class Preloader extends MovieClip
	{
		public static const ENTRY_FRAME:Number = 3;
		public static const DOCUMENT_CLASS:String = "src.game.core.App";
		
		private var progressBar:MovieClip;
		private var progressMask:MovieClip;
		private var logo:MovieClip;
		private var textField:TextField;
		private var btnPlay:MovieClip;
		
		public function Preloader() 
		{
			super();
			stop();
			
			if (atHome(["local", "fgl.com", "www.fgl.com", "flashgamelicense.com", "www.flashgamelicense.com", "www.easyflash.org"])) 
			{
				if (stage)
				{
					init();
				}
				else
				{
					this.addEventListener(Event.ADDED_TO_STAGE, init);
				}
			} 
			else 
			{				
			   trace("Bad Url!!!");
			}	
		}
		
		private function init(e:Event = null):void 
		{	
			stage.scaleMode = StageScaleMode.NO_SCALE; 
			stage.align = StageAlign.TOP_LEFT; 
			
			progressBar = getChildByName("ProgressBar") as MovieClip;
			progressMask = progressBar.Mask;
			progressMask.width = 0;
			logo = getChildByName("Logo") as MovieClip;
			textField = getChildByName("Text") as TextField
			textField.text = "0%";
			
			btnPlay = getChildByName("BtnPlay") as MovieClip;
			btnPlay.visible = false;
			
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			loaderInfo.addEventListener(Event.COMPLETE, completeHandler);			
		}
		
		private function progressHandler(event:ProgressEvent):void 
		{
			var loaded:uint = event.bytesLoaded;
			var total:uint = event.bytesTotal;
			//trace(loaded/total);
			progressMask.width = 345 * loaded / total;
			textField.text = Math.round(loaded / total * 100).toString() + "%";
		}
		
		private function completeHandler(event:Event):void 
		{			
			//TweenLite.to(logo, 0.9, { alpha:0 } );
			//TweenLite.to(textField, 0.9, { alpha:0 } );
			textField.visible = false;
			btnPlay.visible = true;
			btnPlay.buttonMode = true;
			btnPlay.addEventListener(MouseEvent.CLICK, cl);
			
			
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progressHandler);
			loaderInfo.removeEventListener(Event.COMPLETE, completeHandler);	
		}
		
		private function cl(event:MouseEvent):void
		{
			btnPlay.removeEventListener(MouseEvent.CLICK, cl);
			
			TweenLite.to(logo, 0.9, { alpha:0 } );
			TweenLite.to(btnPlay, 0.9, { alpha:0 } );
			TweenLite.to(progressBar, 1, { alpha:0, onComplete:cmp });
		}
		
		private function cmp():void
		{
			if(this.contains(logo)) removeChild(logo);
			if(this.contains(textField)) removeChild(textField);
			if (this.contains(progressBar)) removeChild(progressBar);
			if (this.contains(btnPlay)) removeChild(btnPlay);
				
			progressMask = null;
			progressBar = null;
			logo = null;
			textField = null;
			btnPlay = null;
				
			play();			
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);	
		}
		
		private function enterFrameHandler(event:Event):void 
		{
			if (currentFrame >= Preloader.ENTRY_FRAME) 
			{
				removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
				stop();
				main();
			}
		}
		
		private function main():void 
		{						
			var programClass:Class = loaderInfo.applicationDomain.getDefinition(Preloader.DOCUMENT_CLASS) as Class;
			var program:Sprite = new programClass() as Sprite;
			addChild(program);
		}	
		
      /**
       * Возвращает домен на котором размещена флешка.
       * <p>Внимание: Если флешка размещена на домене второго или третьего уровня то вернется имя домена первого уровня,
       * то есть если ваша игра размещена на "http://cache.armorgames.com/", то результат будет "armorgames.com".</p>
       * 
       * @return      Вернет local если флешка запущена на локальном компьютере или адрес домена первого уровня "domain.com".
       */
      protected function getHome():String
      {
         var url:String = loaderInfo.loaderURL;
         var urlStart:Number = url.indexOf("://") + 3;
         var urlEnd:Number = url.indexOf("/", urlStart);
         var home:String = url.substring(urlStart, urlEnd);
         var LastDot:Number = home.lastIndexOf(".") - 1;
         var domEnd:Number = home.lastIndexOf(".", LastDot) + 1;
         home = home.substring(domEnd, home.length);
         return (home == "") ? "local" : home;
      }
      
      /**
       * Реализация элементарного сайтлока. Пример использования:
       * <p><code>if (atHome([ "local", "mygreatsite.com" ])) {<br>
       *    // ура! можно играть<br>
       * } else {<br>
       *    // нельзя играть<br>
       * }</code></p>
       * 
       * @param   aHomes    Список доменов на которых флешке разрешено находится.
       * @return      Возвращает true если флешка находится на одном из разрешенных доменов.
       */
      protected function atHome(aHomes:Array):Boolean
      {
         return (aHomes.indexOf(getHome()) > -1) ? true : false;
      }
	}
}