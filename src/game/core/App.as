package src.game.core 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.utils.getDefinitionByName;
	import src.framework.animation.AntAnimCache;
	import src.utils.SWFProfiler;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class App extends Sprite
	{
		private var game:Game = null;
		
		public function App() 
		{
			if (stage)
			{
				init();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//SWFProfiler.init(stage, this);
			
			var cache:AntAnimCache = AntAnimCache.getInstance();
			
			var rastrList:Array = [			
									Hero_mc,
									Shield_mc,
									Spot_mc,
									
									Bg_1, Bg_2, Bg_3, Bg_4, Bg_5, Bg_6, Bg_7, Bg_8, Bg_9,
									Bg_11, Bg_12, Bg_13, Bg_14, Bg_15, Bg_16, Bg_17, Bg_18, Bg_19,
									
									BossWhite_mc,
									
									MainMenuPlane1_mc,
									MainMenuPlane2_mc,
									
									Coin_mc,
									Medal_mc,
									BonusShield_mc,
									BonusLive_mc,
									Heart_mc,
									Fog_mc,
									ScreenMarketIconRect_mc,
									ScreenMarketIconHeart_mc,
									ScreenMarketIconBullet_mc,
									
									Heinkel1_mc,
									Heinkel2_mc,
									Heinkel3_mc,
									Heinkel4_mc,
									Heinkel5_mc,
									Horten_mc,
									Dornier_mc,
									Colibri_mc,
									ColibriPlanes_mc,
									BossCenter_mc,
									BossRight_mc,
									BossLeft_mc,
									BossTurret_mc,
									
									BulletH100_mc,
									BulletH200_mc,
									BulletE100_mc,
									BulletE200_mc,
									BulletE300_mc,
									
									ExplosionE50_mc,
									ExplosionE100_mc,
									ExplosionE200_mc,
									ExplosionE300_mc,
									
									MainMenu_background_mc,
									LevelSelect_background_mc,
									Game_background_mc,
									Market_background_mc,
									PanelGameInfo_mc,
									PanelLevelComplete_mc,
									PanelLevelFailed_mc,
									PanelPause_mc,
									
									ButtonCircle_mc,
									ButtonBuy_mc,
									ButtonPlayLevel_mc,
									ButtonMarket_mc,
									ButtonMainOrange_mc,
									ButtonMainBlue_mc,
									ButtonPause_mc
									];			
			
			cache.addClipsToCacheQueue(rastrList);
			
			cache.onProgressCallback = onCacheProgress;
			cache.onCompleteCallback = onCacheComplete;
			
			cache.makeCache();	
		}	
		
		private function onCacheProgress(persent:Number):void 
		{
			//trace(Math.floor(persent * 100) + "%");
		}
		
		private function onCacheComplete():void 
		{			
			game = new Game();
			this.addChild(game);			
		}		
	}
}