package src.game.core.managers 
{
	import src.game.interfaces.windows.BasicWindow;
	import src.game.interfaces.windows.screens.ScreenGame;
	import src.game.interfaces.windows.screens.ScreenLevelSelect;
	import src.game.interfaces.windows.screens.ScreenMainMenu;
	import src.game.interfaces.windows.screens.ScreenMarket;
	/**
	 * ...
	 * @author DANIIL
	 */
	public class WindowsManager 
	{
		private var windows:Array;
		
		private var isSwitch:Boolean;
		private var screenName:int;
		
		private var currentScreen:BasicWindow;
		
		public function WindowsManager() 
		{
			
		}	
		
		public function init():void
		{
			windows = [];
			
			var w:BasicWindow;
			
			w = new ScreenMainMenu();
			w.init();
			currentScreen = w;
			
			w = new ScreenLevelSelect();
			w.init();
			
			w = new ScreenMarket();
			w.init();
			
			w = new ScreenGame();
			w.init();
			
			currentScreen.show();
		}
		
		public function update():void
		{
			if (isSwitch)
			{
				currentScreen.hide();
				if (currentScreen.isHide)
				{
					currentScreen = getWindowByName(screenName);
					currentScreen.show();
					isSwitch = false;
				}
			}
		}
		
		public function addWindow(w:BasicWindow):void
		{
			windows.push(w);
		}
		
		public function switchScreen(name:int):void
		{
			if (!isSwitch)
			{
				isSwitch = true;
				screenName = name;
			}
		}
		
		public function getWindowByName(name:int):BasicWindow
		{
			for (var i:int = 0; i < windows.length; i++)
			{
				if (windows[i].id == name) return windows[i];
			}
			
			return null;
		}
	}
}