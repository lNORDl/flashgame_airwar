package src.game.core.managers 
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import src.game.core.Universe;
	import src.utils.Kind;
	/**
	 * ...
	 * @author D.A.N.I.I.L
	 */
	public class SoundManager extends Object
	{
		private static var _instance:SoundManager;
		
		public var isPlay:Boolean;
		
		private var _soundChannel:SoundChannel = new SoundChannel();
		private var _soundTransform:SoundTransform = new SoundTransform();
		
		private var _mainSoundChanel:SoundChannel = new SoundChannel();
		private var _mainTransform:SoundTransform = new SoundTransform();

		private var soundMenu:Sound;
		private var soundGame:Sound;
		private var mainT:int = -1;
		
		
		public function SoundManager() 
		{
			_instance = this;
		}
		
		public function init():void
		{
			isPlay = true;
			
			_mainTransform.volume = 0.65;
			soundMenu = new SoundMenu();
			soundGame = new SoundGame();
		}
		
		public function playMainSound(t:int, r:Boolean = false):void 
		{
			if (_mainSoundChanel !== null)
			{
				_mainSoundChanel.removeEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);	
			}
			
			if ((!r && t !== mainT) || r)
			{
				_mainSoundChanel.stop();
				_mainSoundChanel = null;
				
				if (t == Kind.S_MENU)
				{
					_mainSoundChanel = soundMenu.play(0, 0, _mainTransform);
					_mainSoundChanel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);	
				}
				else if (t == Kind.S_GAME)
				{
					_mainSoundChanel = soundGame.play(0, 0, _mainTransform);
					_mainSoundChanel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);	
				}			
				
				mainT = t;
				//trace(_mainSoundChanel.rightPeak, _mainSoundChanel.leftPeak);
			}
		}
		
		private function soundCompleteHandler(e:Event):void 
		{
			playMainSound(mainT, true);
		}
		
		public function playSound(tag:int):void
		{
			if (isPlay)
			{
				var _sound:Sound = null;
				
				switch(tag)
				{
					case Kind.S_BUTTON_CLICK:
					_sound = new SoundButtonClick();
					break;
					
					case Kind.S_COIN_PICKUP:
					_sound = new SoundCoinPickup();
					break;
					
					case Kind.S_EXPLOSION_E100:
					_sound = new SoundExplosionE100();
					break;
					
					case Kind.S_EXPLOSION_E200:
					_sound = new SoundExplosionE200();
					break;	
					
					case Kind.S_BONUS_LIVE:
					_sound = new SoundBonusLive();
					break;
					
					case Kind.S_BONUS_SHIELD:
					_sound = new SoundBonusShield();
					break;	
					
					case Kind.S_BUY:
					_sound = new SoundButtonClick();
					break;
					
					case Kind.S_MEDAL:
					_sound = new SoundMedal();
					break;	
				}
				
				if (_sound !== null) _soundChannel = _sound.play(0, 0, _soundTransform);
			}
		}
		
		public function paused(b:Boolean):void
		{
			if (isPlay == b)
			{
				if (b == true)
				{
					isPlay = false;
					
					_soundTransform.volume = 0;					
					_mainTransform.volume = 0;
					
				}
				else
				{
					isPlay = true;
					_soundTransform.volume = 1.0;					
					_mainTransform.volume = 0.65;
				}
				_soundChannel.soundTransform = _soundTransform;
				_mainSoundChanel.soundTransform = _mainTransform;
			}
		}
		
		public static function getInstance():SoundManager
		{
			return (_instance == null) ? new SoundManager() : _instance;
		}
		
	}

}