﻿package  
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.events.ContextMenuEvent;
	/**
	 * ...
	 * @author Jacky Riawan
	 */
	public class preloaderBox10 extends MovieClip
	{
		private var mc1:MovieClip
		private var but:SimpleButton
		private var preload_mc:MovieClip
		private var mainGame:MovieClip;
		private var gameLoaded:Boolean;
		private var gameLink = "http://www.box10.com/" //<- custom link to preloader
		private const timerMax:int =7*24//<-change this for the minimum timer start game to appear
		private var timer:int=0;
		public function preloaderBox10() 
		{
			setupCustomMenu()//<-block right click
			//this.loaderInfo.addEventListener(Event.COMPLETE, finishLoading);
		}
		public function startGameNow() {
			//instructions: 
			//copy instance of #box10PreloaderAS3 movie clip in frame 1 and stop the frame
			//copy this preloader.as in the same folder as your game fla
			
			//
			//mainGame refer to root
			//for example you have a main function called "startGame()" to initiate the game
			//mainGame.startGame()
			//example below is to play() the main timeline
			trace("game loaded starting game")
			mainGame.play()//<-remove this and put your script here
			
		}
		public function getLink(e:MouseEvent):void 
		{
			var myRequest:URLRequest = new URLRequest(gameLink)
			navigateToURL(myRequest,"_blank");
		}
		public function buttonLink(mc) {
			mc.addEventListener(MouseEvent.CLICK, getLink)
		}
		public function init(arr:Array) {
			mainGame=arr[0]
			mc1 = arr[1]
			but = arr[2]
			preload_mc = arr[3];
			//check if the loading time is infinity
			if (this.loaderInfo.bytesLoaded / this.loaderInfo.bytesTotal == Infinity) {
				status_txt.text = "loading...";
				this.loaderInfo.addEventListener(Event.COMPLETE, finishLoading);
			}else {
				status_txt.text = "";
				addEventListener(Event.ENTER_FRAME, startLoading)
			}
		}
		
		private function finishLoading(e:Event):void 
		{
			status_txt.text = "load completed";
			stage.loaderInfo.removeEventListener(Event.COMPLETE, finishLoading)
			mc1.gotoAndStop(mc1.totalFrames);
			gameLoaded=true
			showButton()
		}
		private function startLoading(e:Event):void 
		{
			var persen:Number = 100*stage.loaderInfo.bytesLoaded / stage.loaderInfo.bytesTotal
			timer++;
			var frame:int = Math.min(100,Math.floor(persen),100*timer/timerMax);
			if (frame >= mc1.currentFrame&&!gameLoaded) {
				
				if (mc1.currentFrame == mc1.totalFrames) {
					gameLoaded=true
					showButton()
				}else{
					mc1.nextFrame()
				}
			}else {
				mc1.stop()
			}
		}
		public function showButton() {
			but.visible = true
			removeEventListener(Event.ENTER_FRAME,startLoading)
			but.addEventListener(MouseEvent.CLICK,startGame)
		}
		
		private function startGame(e:MouseEvent):void 
		{
			preload_mc.gotoAndStop(3)
			but.removeEventListener(MouseEvent.CLICK, startGame)
		}
		private function setupCustomMenu():void
		{
			var menu:ContextMenu = new ContextMenu()
			menu.hideBuiltInItems()
			var box10Menu:ContextMenuItem = new ContextMenuItem("Play more games @box10.com",false,true)
			box10Menu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,box10MenuHandler)
			menu.customItems.push(box10Menu)
			this.contextMenu=menu
		}
		private function box10MenuHandler(e:ContextMenuEvent):void 
		{
			var myRequest:URLRequest = new URLRequest(gameLink)
			navigateToURL(myRequest,"_blank");
		}
	}

}